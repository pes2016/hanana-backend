'use strict'

var express = require('express');
const app = express();
var mongoose = require('mongoose');
var config = require('./config.js');
var passport = require('passport');
var bodyParser = require('body-parser');
var morgan = require('morgan');

let uri = '';
if (process.env.NODE_ENV === 'production') {
    uri = process.env.MONGOLAB_URI;
    console.log('===PRODUCTION===');
} else if (process.env.NODE_ENV === 'test') {
    console.log('===TEST===');
} else {
    uri = '127.0.0.1/hanana';
    app.use(morgan('dev'));
    console.log('===DEVELOPMENT===');
}

mongoose.Promise = global.Promise;
mongoose.connect(config.dbUri, {}, (err) => {
    if (err) {
        console.log('Connection Error: ', err);
    } else {
        console.log('Successfully Connected');
    }
});

var routes = require('./routes/index.js')(express);

app.use(config.publicPath, express.static(config.appPath));
app.use('/bower_components', express.static(config.staticPath + 'bower_components'));
app.use('/avatars', express.static('./avatars'));
app.use('/groupImages', express.static('./groupImages'));
app.use('/badgeImages', express.static('./badgeImages'));
app.use('/eventImages', express.static('./eventImages'));
app.use('/images', express.static('./images'));
app.use('/qrCodes', express.static('./qrCodes'));
app.use(passport.initialize());

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({
    extended: false,
    limit: '50mb'
}));
app.use(routes);

let port = process.env.PORT || config.port || 4000;
let host = process.env.IP || config.ip || 4000;
app.listen(port, () => {
    console.log('NODE ENV ', process.env.NODE_ENV);
    console.log('Server started at : ', host + ':' + port);
});

exports.app = app;
