'use strict';

var mongoose = require('mongoose');
var Interest = require('../../models/interest.js');
var config = require('../../config.js');
var _ = require('lodash');

mongoose.connect(config.dbUri);
mongoose.Promise = global.Promise;

var data = [{
    name: 'Música',
    url: 'images/music.jpg'
}, {
    name: 'Gastronomía',
    url: 'images/food.jpg'
}, {
    name: 'Al aire libre',
    url: 'images/nature.jpg'
}, {
    name: 'Familia',
    url: 'images/family.jpeg'
}, {
    name: 'Deportes',
    url: 'images/sports.jpg'
}, {
    name: 'Fiesta',
    url: 'images/party.jpeg'
}, {
    name: 'Cultura',
    url: 'images/culture.jpg'
}, {
    name: 'Geek',
    url: 'images/geek.jpeg'
}, {
    name: 'LGTB',
    url: 'images/lgtb.jpg'
}, {
    name: 'Arte',
    url: 'images/art.jpeg'
}];

Interest.count({}, (err, number) => {
    if (!number) {
        Interest.insertMany(data, (err, docs) => {
            if (err) {
                console.log('Error inserting ', err);
            } else {
                console.log('Inserted ' + docs + ' interests.');
            }

            process.exit(0);
        });
    } else {
        console.log('Interests already exist.');
        var bulk = Interest.collection.initializeOrderedBulkOp();

        for (let item of data) {
            bulk.find({
                name: item.name
            }).upsert().update({
                $set: {
                    url: item.url
                }
            });
        }

        bulk.execute((err, data) => {
            if (err) {
                console.log('Error updating.');
            } else {
                console.log('Interests inserted ', data.nInserted, ' and updated ', data.nModified);
            }

            process.exit(0);
        });
    }
});
