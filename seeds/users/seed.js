'use strict';

var mongoose = require('mongoose');
var User = require('../../models/user.js');
var config = require('../../config.js');
var _ = require('lodash');
var as = require('async');
var bcrypt = require('bcrypt-nodejs');

mongoose.connect(config.dbUri);
mongoose.Promise = global.Promise;
var db = mongoose.connection;

var users = [{
    name: 'admin',
    id: 1,
    email: 'admin@admin.com',
    password: 'adminadmin',
    isAdmin: true,
    isActive: true
}, {
    name: 'premium',
    id: 2,
    email: 'premium@premium.com',
    password: 'premium',
    provider: 'web',
    isPremium: true,
    isActive: true
}];

var orQuery = [];
_.map(users, (user) => {
    orQuery.push({
        name: user.name
    });
});

User.remove({
    $or: orQuery
}, (err, total) => {
    console.log('Previous seed users removed');
    as.eachSeries(users, function iteratee(user, callback) {
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(user.password, salt, null, (err1, hash) => {
                user.password = hash;
                callback();
            });
        });
    }, function done() {

        User.collection.insert(users, (err, docs) => {
            if (err) {
                console.log('An error has ocurred while seeding users');
            }

            console.log('New users seeded.');
            process.exit(0);
        });
    });
});
