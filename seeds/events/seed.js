'use strict'
var mongoose = require('mongoose');
var fs = require('fs');
var User = require('../../models/user.js');
var Group = require('../../models/group.js');
var Event = require('../../models/event.js');
var filePath = 'seeds/events/db.txt';
var config = require('../../config.js');
var path = require('path');
var _ = require('lodash');
function logError(err) {
    if (err) {
        console.log(err);
    }
}

var ObjectID = require('mongodb').ObjectID;
mongoose.connect(config.dbUri);
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', logError);
db.once('open', logError);

fs.readFile(filePath, function (err, data) {
    if (err) {
        console.log(err);
    } else {
        var eventsInfo = JSON.parse(data);
        var promises = [];
        var firstUser = eventsInfo.users[0];
        var firstGroup = eventsInfo.groups[0];
        _.map(eventsInfo.events, (e) => {
            e._id = new ObjectID(e._id);
        });
        delete firstUser._id;
        delete firstGroup._id;

        //Si no existe el grupo, lo crea y lo devuelve
        promises.push(Group.findOneAndUpdate(firstGroup, firstGroup, {
            upsert: true,
            new: true
        }));

        //Si no existe el user, lo crea y lo devuelve
        promises.push(User.findOneAndUpdate(firstUser, firstGroup, {
            upsert: true,
            new: true
        }));

        //Elimina eventos anteriores
        promises.push(Event.remove({}));

        Promise.all(promises)
            .then((values) => {
                // values[0] corresponde al grupo, [1] al user, y [2] a lo que deveulve el remove.
                firstGroup = values[0];
                firstUser = values[1];

                if (!firstGroup.owner) {
                    Group.update({
                        _id: firstGroup._id
                    }, {
                        $set: {
                            owner: firstUser._id
                        }
                    });
                }

                Event.collection.insert(eventsInfo.events, (err, inserted) => {
                    Event.update({}, {
                        $set: {
                            group: firstGroup._id
                        }
                    }, (err, docs) => {
                        if (err) {
                            console.log(err);
                        } else {
                            console.log(inserted.insertedCount + ' objects successfully stored');
                            process.exit();
                        }
                    });
                });
            });
    }
});
