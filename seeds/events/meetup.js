'use strict'
var XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;
var fs = require('fs');
var User = require('../../models/user.js').model('User');
var Group = require('../../models/group.js').model('Group');
var Event = require('../../models/event.js').model('Event');

var cities = Array();
var events = Array();
var groups = Array();
var users = Array();

var path = 'db.txt';

var defaultUser = new User({
    isPremium: true,
    about: 'Profesional en SQL Developes i Words Arts',
    avatar: 'http://www.aragranollers.cat/imatges/noticies/p05-xavier-burgues.jpg',
    name: 'Xavier',
    lastnames: 'BurguesIlla Ham',
    fullname: 'Xavier BurguesIlla Ham'
});

var defaultGroup = new Group({
    name: 'Hanana',
    description: 'Somos Hanana, los desarrolladores de esta App',
    image: 'http://i.imgur.com/G2U72IY.png'
});

function buildJSONAndSave() {
    users.push(defaultUser);
    groups.push(defaultGroup);

    var dataDump = {
        events: events,
        groups: groups,
        users: users
    };

    fs.writeFile(path, JSON.stringify(dataDump), function (err) {
        if (err) {
            console.log(err);
        } else {
            console.log('MeetupData dumped to db.txt file correctly');
        }

    });

}

function getEventsFromCountryAndCity(x) {
    if (x < cities.length) {
        if (cities[x].city === 'Badalona') {
            console.log('Hanana Hax Are Being Burned on The Hells of Mordor.');
        } else {
            console.log('Fetching events from ' + cities[x].city);
        }

        var req = new XMLHttpRequest();
        var uri = 'https://api.meetup.com/2/open_events?key=331d6e5340727d64b1b6' +
        '2292143532c&country=' + cities[x].countryCode + '&city=' + cities[x].city;
        uri = encodeURI(uri);
        req.open('GET', uri, true);

        req.setRequestHeader('Ivan', 'event');
        req.setRequestHeader('Content-Type', 'application/xml');
        var body = '<?xml version="1.0"?><person><name>Ivan Martinez</name></person>';
        req.onreadystatechange = function () {
            if (req.readyState === 4 && req.status === 200) {
                var jsonEvents = JSON.parse(req.responseText);
                for (var i = 0; i < jsonEvents.results.length; ++i) {
                    if ('venue' in jsonEvents.results[i]) {
                        var lat = jsonEvents.results[i].venue.lat;
                        var lon = jsonEvents.results[i].venue.lon;
                        var aux = new Event({
                            lat: lat,
                            lon: lon,
                            title: jsonEvents.results[i].name,
                            description: jsonEvents.results[i].description,
                            date: new Date(jsonEvents.results[i].time),
                            idGroup: defaultGroup._id,
                            image: 'http://i.imgur.com/xb9O5qB.jpg',
                            url: 'http://www.pp.es/',
                            location: {
                                coordinates: [Number(lon), Number(lat)],
                                type: 'Point'
                            },
                            tags: ['tag1', 'tag2', 'tag3', 'tag4', 'tag5', 'tag6', 'tag7', 'tag8']
                        });
                        events.push(aux);
                    }
                }

                getEventsFromCountryAndCity(x + 1);
            }
        };

        req.send(body);
    } else if (x === cities.length) {
        buildJSONAndSave();
        console.log('BurguesIlla Ham Secrets Were Stolen Properly. ' +
        'All your base are belong to us.');
    }
}

function getCitiesFromCountry(countryCode, _callback) {
    var req = new XMLHttpRequest();
    req.open('GET', 'https://api.meetup.com/2/cities?key=331d6e5340727d64b1b' +
    '62292143532c&country=' + countryCode, true);
    req.setRequestHeader('Ivan', 'event');
    req.setRequestHeader('Content-Type', 'application/xml');
    var body = '<?xml version="1.0"?><person><name>Ivan Martinez</name></person>';
    req.onreadystatechange = function () {
        if (req.readyState === 4 && req.status === 200) {
            var jsonCities = JSON.parse(req.responseText);
            for (var i = 0; i < jsonCities.results.length; ++i) {
                cities.push({
                    countryCode: countryCode,
                    city: jsonCities.results[i].city
                });
            }

            _callback();
        }
    };

    req.send(body);
}

function init() {
    console.log('Secret hacking tools are being executed now, do not close the computer please.');
    getCitiesFromCountry('es', getEventsFromCountryAndCity.bind(null, 0));
}

init();
