'use strict'
var jwt = require('jsonwebtoken');
var uuid = require('uuid');
var config = require('../config.js');

var bcrypt = require('bcrypt-nodejs');
var Strategy = require('passport-local');
var User = require('../models/user');

module.exports = function (passport) {
    passport.use('local',
        new Strategy({
            // Login with email instead of username(default)
            usernameField: 'email',
            passwordField: 'password'
        }, (email, password, done) => {
            User.findOne({
                email: email,
                isActive: true
            }, (err, user) => {
                if (err) {
                    return done(err);
                }

                if (!user) {
                    return done(null, false);
                }

                bcrypt.compare(password, user.password, (error, res) => {
                    if (res) {
                        return done(null, user);
                    }

                    if (error) {
                        return done(error);
                    }

                    return done(null, null);
                });
            });
        }));
};
