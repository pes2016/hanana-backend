'use strict'

var login = require('../../handlers/login-handler.js');

describe('createPremiumUser', function () {

    var agent;

      before(function (done) {
        login.logAsAdmin(function (loginAgent, token) {
          agent = loginAgent;
          done();
        });
      });

      after(function () {
      });

    it('should POST new premium user', (done) => {
        agent
            .post('/api/admin/registerPremium')
            .query({ access_token: agent.getAC() })
            .send({
                    name: 'xavier',
                    email: 'never4get@sql.developer'
            })
            .expect(200)
            .end((err, res) => {
                if (err) {
                    throw err;
                }

                done();
            });

    });

    it('should FAIL if no data is sent', (done) => {
        agent
        .post('/api/admin/registerPremium')
            .query({ access_token: agent.getAC() })
            .send({
                    name: 'xavier',
            })
            .expect(400, 'Please fill correctly all the required fields.')
            .end((err, res) => {
                if (err) {
                    throw err;
                }

                done();
            });

        agent
        .post('/api/admin/registerPremium')
            .query({ access_token: agent.getAC() })
            .send({
                    email: 'neve22r42get@sql.developer'
            })
            .expect(400, 'Fill the fields correctly to register.')
            .end((err, res) => {
                if (err) {
                    throw err;
                }
            });
    });

    it('should FAIL if email is not correct', (done) => {
        agent
        .post('/api/admin/registerPremium')
            .query({ access_token: agent.getAC() })
            .send({
                    name: 'xavier',
                    email: 'incomplete@email'
            })
            .expect(400, 'Email is incorrect.')
            .end((err, res) => {
                if (err) {
                    throw err;
                }

                done();
            });

        });

    it('should FAIL if already exists user with that email', (done) => {

        agent
        .post('/api/admin/registerPremium')
            .query({ access_token: agent.getAC() })
            .send({
                    name: 'xavier',
                    email: 'never4get@sql.developer'
            })
            .expect(400, 'Already exists a user with that email.')
            .end((err, res) => {
                if (err) {
                    throw err;
                }

                done();
            });

        });

});
