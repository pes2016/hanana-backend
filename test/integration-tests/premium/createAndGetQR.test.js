'use strict'

var login = require('../../handlers/login-handler.js');
var Group = require('../../../models/group');
var Event = require('../../../models/event');
var fs = require('fs');

describe('createAndGetQR', function () {

    var agent;
    var gp;

      before(function (done) {
        login.logAsPremium(function (loginAgent, token) {
          agent = loginAgent;
          gp = new Group({ name: 'Test-group', owner: agent.getID() });
          gp.save(function () {
              done();
          });
        });
      });

      after(function (done) {
          gp.remove(function () {
              done();
          });
      });

    it('we can get a qr from an event created with qr', (done) => {
        agent
            .post('/api/premium/createEvent')
            .query({ access_token: agent.getAC() })
            .field('title', 'Test')
            .field('description', 'This is a test event')
            .field('url', 'test')
            .field('date', (new Date()).toString())
            .field('tags', 'Test')
            .field('group', gp._id.toString())
            .field('qr', 1)
            .field('price', 2)
            .field('age', 10)
            .field('lat', 15.5)
            .field('lon', 15.5)
            .attach('image', './test/files/sql.jpg')
            .expect(200)
            .end((err, evRes) => {
                if (err) {
                    throw(err);
                }

                agent
                    .get('/api/premium/getQR/' + evRes.body)
                    .query({ access_token: agent.getAC() })
                    .expect(200)
                    .end((err, qrRes) => {
                        if (err) {
                            throw err;
                        }

                        assert(qrRes.text, 'No qr path returned.');

                        Event.findById(evRes.body, function (err, ev) {
                            fs.unlink(ev.image),
                            fs.unlink(ev.qrCode);
                            done();
                        });
                    });
            });
    });

    it('should FAIL if we try to get qr of event without qr', (done) => {
        agent
            .post('/api/premium/createEvent')
            .query({ access_token: agent.getAC() })
            .field('title', 'Test')
            .field('description', 'This is a test event')
            .field('url', 'test')
            .field('date', (new Date()).toString())
            .field('tags', 'Test')
            .field('group', gp._id.toString())
            .field('qr', '0')
            .field('price', 2)
            .field('age', 10)
            .field('lat', 15.5)
            .field('lon', 15.5)
            .attach('image', './test/files/sql.jpg')
            .expect(200)
            .end((err, evRes) => {
                if (err) {
                    throw(err);
                }

                agent
                    .get('/api/premium/getQR/' + evRes.body)
                    .query({ access_token: agent.getAC() })
                    .expect(400, 'That event doesn\'t have a QR code.')
                    .end((err, qrRes) => {
                        if (err) {
                            throw err;
                        }

                        Event.findById(evRes.body, function (err, ev) {
                            fs.unlink(ev.image);
                            done();
                        });
                    });
            });
    });

});
