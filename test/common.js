'use strict';

global.chai = require('chai');
global.chai.should();
global.assert = require('chai').assert;

global.expect = global.chai.expect;
global.sinon = require('sinon');

global.sinonChai = require('sinon-chai');
global.chai.use(global.sinonChai);

process.env.NODE_ENV    = 'test';
process.env.MONGO_URL = 'mongodb://localhost/testing';
