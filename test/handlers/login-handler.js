'use strict'

var request = require('supertest');
var server = require('../../server').app;
var agent = request.agent(server);
var adminAccount = {
    email: 'admin@admin.com',
    password: 'adminadmin'
};
var premiumAccount = {
    email: 'premium@premium.com',
    password: 'premium'
};
var User = require('../../models/user');

function log(account, done) {
    agent
        .post('/api/login')
        .send(account)
        .expect(200)
        .end(function (err, res) {
          if (err) {
            throw err;
          }

          var id;
          agent.getAC = function () {
              return res.body.access_token;
          };

          agent.getID = function () {
             return id;
          };

          User.findOne({ access_token: agent.getAC() }, function (err, user) {
              id =  user._id;
              done(agent);
          });
      });
  }

module.exports = {

    logAsAdmin(done) {
        log(adminAccount, done);
    },

    logAsPremium(done) {
        log(premiumAccount, done);
    }

};
