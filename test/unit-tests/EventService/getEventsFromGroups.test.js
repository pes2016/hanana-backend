'use strict'

const EventService = require('../../../services/EventService');
const Event = require('../../../models/event');
const ObjectId = require('mongoose').Types.ObjectId;

var dropables;

beforeEach(function () {
    dropables = sinon.collection;
});

afterEach(function () {
  dropables.restore();
});

describe('EventService', () => {

    describe('getEventsFromGroups', () => {

        let mockFind = {
            populate: function () {
                return this;
            },

            sort: function () {
                return this;
            }
        };

        let mockEvent1 = {
            _id: '5846f3a6ce310357bca68888',
            id: '1',
            title: 'Event Test',
            description: 'This is a event Test',
            date: new Date('October 13, 2014 11:13:00'),
            lat: '50',
            lon: '50',
            address: 'Fake avenue',
            tags: ['Test'],
            age: '18',
            price: '5',
            image: '',
            isActive: true,
            url: '',
            group: new ObjectId('5846f3a6ce310357bca64444'),
            isPrivate: false
        };

        let mockEvent2 = {
            _id: '5846f3a6ce310357bca69999',
            id: '2',
            title: 'Event Test 2',
            description: 'This is a event Test 2',
            date: new Date('October 13, 2014 11:13:00'),
            lat: '50',
            lon: '50',
            address: 'Fake avenue 2',
            tags: ['Test'],
            age: '18',
            price: '5',
            image: '',
            isActive: true,
            url: '',
            group: new ObjectId('5846f3a6ce310357bca65555'),
            isPrivate: false
        };

        let mockList = [mockEvent1, mockEvent2];
        let mockIDs = ['5846f3a6ce310357bca64444', '5846f3a6ce310357bca65555'];

        beforeEach(function () {
            dropables.stub(Event, 'find').returns(mockFind);
        });

        it('should return an event if idList is only one group id', function () {

            mockFind.exec = function (callback) {
                callback(null, [mockEvent1]);
            };

           return EventService.getEventsFromGroups(mockIDs[0]).then(function (events) {
                expect(Event.find).to.have.been.calledOnce;
                expect(Event.find).to.have.been.calledWith({ isActive: true,
                    group: mockIDs[0] }
                );
                assert.deepEqual(events, [mockEvent1], 'Event returned is not correct.');
                expect(events).to.be.instanceof(Array);
                expect(events).to.have.lengthOf(1);
            },

            function (err) {
                console.log(err);
                assert(false, 'should not throw error');
            });
        });

        it('should return an event list if idList is an array of group ids', function () {

            mockFind.exec = function (callback) {
                callback(null, mockList);
            };

            return EventService.getEventsFromGroups(mockIDs).then(function (events) {
                expect(Event.find).to.have.been.calledOnce;
                expect(Event.find).to.have.been.calledWith({ isActive: true,
                    group: { $in: mockIDs }
                });
                assert.strictEqual(events, mockList, 'Event returned is not correct.');
                expect(events).to.be.instanceof(Array);
                expect(events).to.have.lengthOf(2);
            },

            function (err) {
                console.log(err);
                assert(false, 'should not throw error');
            });
        });

        it('should return error if not events found(idList = String)', () => {

            mockFind.exec = function (callback) {
                    callback();
            };

            return EventService.getEventsFromGroups(mockIDs[0]).then(function (event) {
                assert(false, 'should throw error');
            },

            function (err) {
                expect(Event.find).to.have.been.calledOnce;
                expect(Event.find).to.have.been.calledWith({ isActive: true,
                    group: mockIDs[0] }
                );
                assert.strictEqual(err, 1, 'Error code is not correct');
            });
        });

        it('should return error if not events found(idList = Array)', () => {

            mockFind.exec = function (callback) {
                    callback();
            };

            return EventService.getEventsFromGroups(mockIDs).then(function (event) {
                assert(false, 'should throw error');
            },

            function (err) {
                expect(Event.find).to.have.been.calledOnce;
                expect(Event.find).to.have.been.calledWith({ isActive: true,
                    group: { $in: mockIDs }
                });
                assert.strictEqual(err, 1, 'Error code is not correct');
            });
        });

        it('should return error if there\'s an internal error(idList = String)', () => {

            mockFind.exec = function (callback) {
                    callback('    ✓');
            };

            return EventService.getEventsFromGroups(mockIDs[0]).then(function (event) {
                assert(false, 'should throw error');
            },

            function (err) {
                expect(Event.find).to.have.been.calledOnce;
                expect(Event.find).to.have.been.calledWith({ isActive: true,
                    group: mockIDs[0] }
                );
                assert.strictEqual(err, 5, 'Error code is not correct');
            });
        });

        it('should return error if there\'s an internal error(idList = Array)', () => {

            mockFind.exec = function (callback) {
                    callback('    ✓');
            };

            return EventService.getEventsFromGroups(mockIDs).then(function (event) {
                assert(false, 'should throw error');
            },

            function (err) {
                expect(Event.find).to.have.been.calledOnce;
                expect(Event.find).to.have.been.calledWith({ isActive: true,
                    group: { $in: mockIDs }
                });
                assert.strictEqual(err, 5, 'Error code is not correct');
            });
        });
    });
});
