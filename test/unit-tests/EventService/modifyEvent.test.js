'use strict'

const EventService = require('../../../services/EventService');
const Event = require('../../../models/event');
const fs = require('fs');

var dropables;

beforeEach(function () {
    dropables = sinon.collection;
});

afterEach(function () {
  dropables.restore();
});

describe('EventService', () => {

    describe('modifyEvent', () => {

        let eventId = '123456';

        let mockFind = {
            select: function () {
                return this;
            },

            populate: function () {
                return this;
            },

            sort: function () {
                return this;
            }
        };

        let mockEvent1 = {
            _id: '5846f3a6ce310357bca68888',
            id: '1',
            title: 'Event Test',
            description: 'This is a event Test',
            date: new Date('October 13, 2014 11:13:00'),
            lat: '50',
            lon: '50',
            address: 'Fake avenue',
            tags: ['Test'],
            age: '18',
            price: '5',
            image: 'fwf3sa43a3',
            isActive: true,
            url: '',
            group: { name: 'test group' },
            isPrivate: false,
            save: function (callback) {
                return callback();
            }
        };

        let mockEvent2 = {
            _id: '5846f3a6ce310357bca69999',
            id: '2',
            title: 'Event Test 2',
            description: 'This is a event Test 2',
            date: new Date('October 13, 2014 11:13:00'),
            lat: '50',
            lon: '50',
            address: 'Fake avenue 2',
            tags: ['Test'],
            age: '18',
            price: '5',
            image: 'afwadfafw',
            isActive: true,
            url: '',
            group: { name: 'test group' },
            isPrivate: false
        };

        let mockfile = {
            path: 'asdf'
        };

        beforeEach(function () {
            dropables.stub(Event, 'findById').returns(mockFind);
            dropables.stub(fs, 'createWriteStream');
            dropables.stub(fs, 'unlink');
         });

        it('should modify the Event without new file', function () {
            mockFind.exec = function (callback) {
                callback(null, mockEvent1);
            };

            return EventService.modifyEvent(mockEvent1._id, mockEvent1).then(function () {
                expect(Event.findById).to.have.been.calledOnce;
                expect(Event.findById).to.have.been.calledWith(mockEvent1._id);
            },

            function (err) {
                console.log(err);
                assert(false, 'should not throw error');
            });
        });

        it('should modify the Event with new file', function () {
            return EventService.modifyEvent(mockEvent1._id, mockEvent1, mockfile).then(function () {
                expect(Event.findById).to.have.been.calledOnce;
                expect(Event.findById).to.have.been.calledWith(mockEvent1._id);
                expect(fs.unlink).to.have.been.calledOnce;
            },

            function (err) {
                console.log(err);
                assert(false, 'should not throw error');
            });
        });

        it('should return error if there\'s an internal error', () => {
            mockFind.exec = function (callback) {
                    callback('    ✓');
            };

            return EventService.modifyEvent(mockEvent1._id, mockEvent1).then(function () {
                assert(false, 'should throw error');
            },

            function (err) {
                expect(Event.findById).to.have.been.calledOnce;
                expect(Event.findById).to.have.been.calledWith(mockEvent1._id);
                assert.strictEqual(err, 5, 'Error code is not correct');
            });
        });
    });
});
