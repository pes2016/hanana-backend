'use strict'

const EventService = require('../../../services/EventService');
const Event = require('../../../models/event');
const ObjectId = require('mongoose').Types.ObjectId;

var dropables;

beforeEach(function () {
    dropables = sinon.collection;
});

afterEach(function () {
  dropables.restore();
});

describe('EventService', () => {

    describe('getQR', () => {

        let userId = new ObjectId('5846f3a6ce310357bca68373');
        let eventId = '123456';
        let eventQrCode = 'qrCodes/abc123.png';

        let mockFindById = {
            populate: function () {
                return this;
            }
        };

        beforeEach(function () {
            dropables.stub(Event, 'findById').returns(mockFindById);
        });

        it('should return path to qr image if event exists and user is owner', () => {
            mockFindById.exec = function (callback) {
               callback(null, { qrCode: eventQrCode, group: { owner: userId } });
           };

            return EventService.getQR(eventId, userId).then(function (qr) {
                expect(Event.findById).to.have.been.calledOnce;
                expect(Event.findById).to.have.been.calledWith(eventId);
                assert.strictEqual(qr, eventQrCode, 'Qr returned is not correct.');
            },

            function (err) {
                console.log(err);
                assert(false, 'should not throw error');
            });
        });

        it('should return error if event doesn\'t exist', () => {

            mockFindById.exec = function (callback) {
                    callback();
            };

            return EventService.getQR(eventId, userId).then(function (qr) {
                assert(false, 'should throw error');
            },

            function (err) {
                expect(Event.findById).to.have.been.calledOnce;
                expect(Event.findById).to.have.been.calledWith(eventId);
                assert.strictEqual(err, 1, 'Error code is not correct');
            });
        });

        it('should return error if there\'s an internal error', () => {

            mockFindById.exec = function (callback) {
                    callback('    ✓');
            };

            return EventService.getQR(eventId, userId).then(function (qr) {
                assert(false, 'should throw error');
            },

            function (err) {
                expect(Event.findById).to.have.been.calledOnce;
                expect(Event.findById).to.have.been.calledWith(eventId);
                assert.strictEqual(err, 5, 'Error code is not correct');
            });
        });

        it('should return error if the event doesn\'t belong to the user', () => {

            mockFindById.exec = function (callback) {
                callback(null, { qrCode: eventQrCode, group:
                    { owner: new ObjectId('666666666666666666666666') } });
            };

            return EventService.getQR(eventId, userId).then(function (qr) {
                assert(false, 'should throw error');
            },

            function (err) {
                expect(Event.findById).to.have.been.calledOnce;
                expect(Event.findById).to.have.been.calledWith(eventId);
                assert.strictEqual(err, 2, 'Error code is not correct');
            });
        });
    });

});
