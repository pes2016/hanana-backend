'use strict'

const EventService = require('../../../services/EventService');
const Event = require('../../../models/event');
const Group = require('../../../models/group');
const fs = require('fs');
const qr = require('qr-image');
const uuid = require('uuid');
const qrCodesPath = require('../../../config.js').QRImageStorage;

var dropables;

beforeEach(function () {
    dropables = sinon.collection;
});

afterEach(function () {
  dropables.restore();
});

describe('EventService', () => {

    describe('createEvent', () => {

        let eventId = '123456';

        beforeEach(function () {
            dropables.stub(Event, 'create').yields(null, { _id: eventId });
            dropables.stub(Event, 'update');
            dropables.stub(fs, 'createWriteStream');
            dropables.stub(Group, 'update');
            dropables.stub(uuid, 'v1').returns('qrFile');
            dropables.stub(qr, 'image').returns({ pipe: function () {}
         });
        });

        it('should generate a new QR and store it on a file if qr == true', function () {
            return EventService.createEvent({ qr: '1' }).then(function (id) {
                expect(qr.image).to.have.been.calledOnce;
                expect(qr.image).to.have.been.calledWith(eventId);
                expect(uuid.v1).to.have.been.calledOnce;
                expect(fs.createWriteStream).to.have.been.calledOnce;
                expect(fs.createWriteStream).to.have.been.calledWith(qrCodesPath + 'qrFile.png');
            },

            function (err) {
                console.log(err);
                assert(false, 'should not throw error');
            });
        });

        it('should not generate qr or store it if qr == false', function () {
            return EventService.createEvent({ qr: '0' }).then(function (id) {
                expect(qr.image).to.not.have.been.called;
                expect(fs.createWriteStream).to.not.have.been.called;
            },

            function (err) {
                console.log(err);
                assert(false, 'should not throw error');
            });
        });
    });
});
