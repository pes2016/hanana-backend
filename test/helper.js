'use strict'
var mongoose = require('mongoose');
const spawn = require('child_process').spawn;
var seedUsers = spawn('node', ['seeds/users/seed.js']);

before(function (done) {
    console.log('Filling test database');
    seedUsers.on('close', function () {
        done();
    });
});

after(function (done) {
    console.log('Deleting test database');
    mongoose.connection.db.dropDatabase(done);
});
