'use strict'
var Interest = require('../models/interest');

module.exports = {
    listAll: function listAll(req, res) {
        Interest.find({}, (err, docs) => {
            if (err) {
                return res.status(400).send();
            }

            return res.json(docs);
        });
    }
};
