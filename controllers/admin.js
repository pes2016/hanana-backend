'use strict'
var UserService = require('../services/UserService');
var BadgeService = require('../services/BadgeService');
var ComplaintService = require('../services/ComplaintService');
var validations = require('../helpers/validations');
var lang = require('../handlers/language-handler');
var errHelper = require('../helpers/error-helper');
const NotificationService = require('../services/NotificationService');

module.exports = {

    getPremiumUsers: function (req, res) {
        UserService.getPremiumUsers()
             .then(function (objs) {
                 return res.status(200).send(objs);
             },

             function (err) {
                 errHelper.getErrorDataFromCode(err, req.query.lang, 'findPremium')
                 .then(function (err) {
                     return res.status(err.code).send(err.message);
                 });
             });
    },

    getPremiumUser: function (req, res) {
        UserService.getPremiumUsers(req.params.id)
             .then(function (obj) {
                 return res.status(200).send(obj);
             },

             function (err) {
                 errHelper.getErrorDataFromCode(err, req.query.lang, 'findPremium')
                 .then(function (err) {
                     return res.status(err.code).send(err.message);
                 });
             });
    },

    editPremiumUser: function (req, res) {
        UserService.modifyUser(req.params.id, req.body)
             .then(function (obj) {
                 return res.status(200).send(obj);
             },

             function (err) {
                 errHelper.getErrorDataFromCode(err, req.query.lang, 'findPremium')
                 .then(function (err) {
                     return res.status(err.code).send(err.message);
                 });
             });
    },

    removePremiumUser: function (req, res) {
        UserService.removeUser(req.params.id)
             .then(function (removed) {
                 return res.status(200).send(removed);
             },

             function (err) {
                 errHelper.getErrorDataFromCode(err, req.query.lang, 'findPremium')
                 .then(function (err) {
                     return res.status(err.code).send(err.message);
                 });
             });
    },

    resetPassword: function (req, res) {
        UserService.resetPassword(req.params.id)
             .then(function () {
                 return res.status(200).send();
             },

             function (err) {
                 errHelper.getErrorDataFromCode(err, req.query.lang, 'findPremium')
                 .then(function (err) {
                     return res.status(err.code).send(err.message);
                 });
             });
    },

    registerPremiumUser: function registerPremiumUser(req, res) {
        if (!req.body || !req.body.name || !req.body.email) {
            return res.status(400).send(lang.getStringFromLang(req.query.lang, 'fillAllFields'));
        }

        if (!validations.emailValidation(req.body.email)) {
            return res.status(400).send(lang.getStringFromLang(req.query.lang,
                 'registerPremiumUser', 'email'));
        }

        UserService.createPremiumUser(req.body.name, req.body.email)
             .then(function (id) {
                 return res.status(200).send(id);
             },

             function (err) {
                 errHelper.getErrorDataFromCode(err, req.query.lang, 'findPremium')
                 .then(function (err) {
                     return res.status(err.code).send(err.message);
                 });
             });
    },

    getBadges: function (req, res) {
        BadgeService.getBadges(req.user, req.query.lang).then(function (badges) {
            return res.status(200).send(badges);
        },

        function (err) {
            errHelper.getErrorDataFromCode(err, req.query.lang, 'findBadge')
            .then(function (err) {
                return res.status(err.code).send(err.message);
            });
        });
    },

    getBadge: function (req, res) {
        var id = req.params.id;
        BadgeService.getBadge(req.user, id, req.query.lang).then(function (badges) {
            return res.status(200).send(badges);
        },

        function (err) {
            errHelper.getErrorDataFromCode(err, req.query.lang, 'findBadge')
            .then(function (err) {
                return res.status(err.code).send(err.message);
            });
        });
    },

    createBadge: function (req, res) {

        if (!req.file) {
            return res.status(400).send(lang.getStringFromLang(req.query.lang, 'nofile'));
        }

        var data = req.body;

        if (!data.description || !data.name ||  !req.file.path ||
            !data.points || !data.trigger || !data.triggerValue ||
            !parseInt(data.triggerValue, 10) ||
            !parseInt(data.points, 10)) {
            return res.status(400).send(lang.getStringFromLang(req.query.lang, 'fillAllFields'));
        }

        if (data.trigger !== 0 && !parseInt(data.triggerValue, 10)) {
            return res.status(400).send(lang.getStringFromLang(req.query.lang, 'fillAllFields'));
        }

        var badge = Object.assign(data, { image: req.file.path });

        BadgeService.createBadge(badge, req.user._id).then(function (badge) {
            return res.status(200).send(badge);
        },

        function (err) {
            errHelper.getErrorDataFromCode(err, req.query.lang, 'createBadge')
            .then(function (err) {
                return res.status(err.code).send(err.message);
            });
        });
    },

    editBadge: function (req, res) {

        var data = req.body;
        var badgeId = req.params.id;

        if (!data || !badgeId) {
            return res.status(400).send(lang.getStringFromLang(req.query.lang, 'fillOneField'));
        }

        if (data.trigger && data.trigger !== 0 && !parseInt(data.triggerValue, 10)) {
            return res.status(400).send(lang.getStringFromLang(req.query.lang, 'fillAllFields'));
        }

        var badge = data;
        if (req.file) {
            badge = Object.assign(data, { image: req.file.path });
        }

        BadgeService.editBadge(badgeId, badge, req.user._id).then(function (badge) {
            return res.status(200).send(badge);
        },

        function (err) {
            errHelper.getErrorDataFromCode(err, req.query.lang, 'findBadge')
            .then(function (err) {
                return res.status(err.code).send(err.message);
            });
        });
    },

    getTriggers: function (req, res) {
        BadgeService.getTriggers(req.user, req.query.lang).then(function (triggers) {
            return res.status(200).send(triggers);
        });
    },

    getComplaints: function (req, res) {
        ComplaintService.getComplaints(req.query.page).then(function (complaints) {
            return res.status(200).send(complaints);
        },

        function (err) {
            errHelper.getErrorDataFromCode(err, req.query.lang, 'findComplaint')
            .then(function (err) {
                return res.status(err.code).send(err.message);
            });
        });
    },

    discardComplaint: function (req, res) {
        ComplaintService.discardComplaint(req.params.id).then(function () {
            return res.status(200).send();
        },

        function (err) {
            errHelper.getErrorDataFromCode(err, req.query.lang, 'findComplaint')
            .then(function (err) {
                return res.status(err.code).send(err.message);
            });
        });
    },

    acceptComplaint: function (req, res) {
        ComplaintService.acceptComplaint(req.params.id, req.query.type).then(function (removed) {
            return res.status(200).send(removed);
        },

        function (err) {
            errHelper.getErrorDataFromCode(err, req.query.lang, 'findComplaint')
            .then(function (err) {
                return res.status(err.code).send(err.message);
            });
        });
    },

    notifyUsers: function (req, res) {
        NotificationService.notifyAll(req.body).then(
          function (values) {
              res.json(values);
          },

          function (error) {
              res.status(500).send(error);
          }
      );
    }
};
