'use strict';
var Group = require('../models/group');
var paging = require('../handlers/paging-handler');
var _ = require('lodash');
var EventsFinderService = require('../services/EventsFinderService');
var Utils = require('../helpers/utils');
var as = require('async');
const AppGroupService = require('../services/AppGroupService');
const Triggers = require('../triggers/executer');

module.exports = {

    discoverGroups: function (req, res) {
        AppGroupService.discoverGroups(req.body, req.user)
            .then(
            function (values) {
                return res.json(values);
            },

            function (err) {
                return res.status(500).send(err);
            }
        );
    },

    getGroup: function (req, res) {
        AppGroupService.getGroup(req.params.id, req.user, req.body)
            .then(
            function (values) {
                return res.json(values);
            },

            function (err) {
                return res.status(500).send(err);
            }
        );
    },

    toggleFollow: function (req, res) {
        AppGroupService.toggleFollow(req.params.id, req.user, req)
            .then(
            function (isFollowing) {
                return res.json(isFollowing);
            },

            function (err) {
                return res.status(500).send(err);
            }
        );
    },

    getFollowers: function (req, res) {
        AppGroupService.getFollowers(req.params.id, req.user, req.body)
            .then(
            function (isFollowing) {
                return res.json(isFollowing);
            },

            function (err) {
                return res.status(500).send(err);
            }
        );
    },

    shareGroup: function shareGroup(req, res) {
        console.log('share group ');
        AppGroupService.shareGroup(req.params.id, req)
            .then(
            function (values) {
                return res.json(values);
            },

            function (err) {
                return res.status(500).send(err);
            }
        );
    }

};
