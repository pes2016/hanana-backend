'use strict';
const Event = require('../models/event');
const _ = require('lodash');
const paging = require('../handlers/paging-handler');
const Utils = require('../helpers/utils');
const fs = require('fs');
const EventQueryHelper = require('../helpers/event-query-helper');
const AppEventService = require('../services/AppEventService');
const PrivateEventService = require('../services/PrivateEventService');
const EventsFinderService = require('../services/EventsFinderService');

function getGeolocatedEvents(req, res) {
    promiseExecutor(EventsFinderService.getGeolocatedEvents(req.body, req.user), res);
}

function getPopularEvents(req, res) {
    promiseExecutor(EventsFinderService.getPopularEvents(req.body, req.user), res);
}

function getSuggestedEvents(req, res) {
    promiseExecutor(EventsFinderService.getSuggestedEvents(req.body, req.user), res);
}

function getFriendsEvents(req, res) {
    promiseExecutor(EventsFinderService.getFriendsEvents(req.body, req.user), res);
}

function createEvent(req, res) {
    promiseExecutor(PrivateEventService.createEvent(req.body, req.user._id, req), res);
}

function updateEventImage(req, res) {
    if (!req.file) {
        console.log('no file provided');
        return res.status(400).send('No file provided');
    }

    promiseExecutor(PrivateEventService.updateEventImage(req.params.id, req.file), res);
}

function confirmQR(req, res) {
    promiseExecutor(AppEventService.confirmQR(req.params.id, req.user._id, req), res);

}

function getEvent(req, res) {
    promiseExecutor(AppEventService.getEvent(req.params.id, req.user), res);
}

function toggleAssist(req, res) {
    promiseExecutor(AppEventService.toggleAssist(req.params.id, req.user._id), res);
}

function rateEvent(req, res) {
    promiseExecutor(AppEventService.rateEvent(req.params.id, req.user._id, req.body), res);
}

function getAssistants(req, res) {
    promiseExecutor(AppEventService.getAssistants(req.params.id, req.user._id, req.body), res);
}

function shareEvent(req, res) {
  promiseExecutor(AppEventService.shareEvent(req.params.id, req), res);
}

function voteRate(req, res) {
  promiseExecutor(AppEventService.voteRate(req.params.id, req.params.rateId,
      req.params.type, req.user), res);
}

function getRatesForEvent(req, res) {
  promiseExecutor(AppEventService.getRatesForEvent(req.params.id, req.user, req.body), res);
}

function promiseExecutor(p, res) {
    p.then(function (values) {
            console.log('okai');
            return res.json(values);
        },

        function (err) {
            console.log('error ', err);
            return res.status(500).json(err);
        }
    );
}

module.exports = {
    getEvent: getEvent,
    toggleAssist: toggleAssist,
    confirmQR: confirmQR,
    createEvent: createEvent,
    updateEventImage: updateEventImage,
    rateEvent: rateEvent,
    getGeolocatedEvents: getGeolocatedEvents,
    getSuggestedEvents: getSuggestedEvents,
    getPopularEvents: getPopularEvents,
    getFriendsEvents: getFriendsEvents,
    getAssistants: getAssistants,
    shareEvent: shareEvent,
    voteRate: voteRate,
    getRatesForEvent: getRatesForEvent
};
