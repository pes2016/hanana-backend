'use strict'
const AppUserService = require('../services/AppUserService');
const UsersInteractionService = require('../services/UsersInteractionService');
const AppUserAccountService = require('../services/AppUserAccountService');
const BadgeService = require('../services/BadgeService');
const UserService = require('../services/UserService');
var lang = require('../handlers/language-handler');
var errHelper = require('../helpers/error-helper');

module.exports = {

    //SELF ACCOUNT INFO
    updateAvatar: function updateAvatar(req, res) {
        if (!req.file) {
            return res.status(400).send('No file provided');
        }

        AppUserAccountService.updateAvatar(req.user, req.file, req)
            .then(
                function (avatarPath) {
                    return res.status(200).send(avatarPath);
                },

                function (err) {
                    return res.status(500).send(err);
                }
            );

    },

    closeAccount: function closeAccount(req, res) {
        AppUserAccountService.closeAccount(req.user)
            .then(function (value) {
                return res.status(200).send();
            }, function (err) {

                return res.status(500).send(err);
            });
    },

    togglePrivacity: function togglePrivacity(req, res) {
        AppUserAccountService.togglePrivacity(req.user)
            .then(
                function (values) {
                  return res.status(200).send(values);
                },

                function (err) {
                    return res.status(500).send(err);
                }
            );
    },

    updateAbout: function setBio(req, res) {
        AppUserAccountService.updateAbout(req.user, req.body.about, req)
            .then(
                function (values) {
                  return res.status(200).send(values);
                },

                function (err) {
                    return res.status(500).send(err);
                }
            );
    },

    updateInterests: function updateInterests(req, res) {
        let interests = req.body || [];
        AppUserAccountService.updateInterests(req.user, interests, req)
            .then(
                function (values) {
                    return res.status(200).send(values);
                },

                function (err) {
                    return res.status(500).send(err);
                });
    },

    me: function me(req, res) {
        //As the user is logged, we can simply return it
        res.json(req.user);
    },

    //GET USER LISTS
    getUserProfile: function (req, res) {
        let me = req.user;
        let userIdTofind = req.params.id || me._id;
        AppUserService.getUserProfile(userIdTofind, me, req.body)
            .then(function (values) {
                    return res.status(200).send(values);
                },

                function (err) {
                    console.log('error ', err);
                    return res.status(500).send(err);
                });
    },

    getUserInterests: function (req, res) {
        let userId = req.params.id || req.user._id;
        AppUserService.getUserInterests(userId)
            .then(function (values) {
                return res.status(200).send(values);
            }, function (err) {

                return res.status(500).send(err);
            });
    },

    getUsers: function (req, res) {
        AppUserService.getUsers(req.body, req.user)
            .then(
                function (values) {
                    return res.json({
                        users: values
                    });
                },

                function (err) {
                    return res.status(500).send(err);
                }
            );
    },

    getBlockedUsers: function (req, res) {
        AppUserService.getBlockedUsers(req.user)
            .then(
                function (result) {
                    res.json({
                        users: result
                    });
                },

                function (err) {
                    console.log('error is ', err);
                    res.status(500).send(err);
                });
    },

    getUserFollowers: function (req, res) {
        AppUserService.getUserFollowers(req.params.id, req.user)
            .then(
                function (result) {
                    res.json(result);
                },

                function (err) {
                    console.log('error is ', err);
                    res.status(500).send(err);
                });
    },

    getUserFollowing: function (req, res) {
        AppUserService.getUserFollowing(req.params.id, req.user)
            .then(
                function (result) {
                    res.json(result);
                },

                function (err) {
                    console.log('error is ', err);
                    res.status(500).send(err);
                });
    },

    getUserPendingRequests: function (req, res) {
        AppUserService.getUserPendingRequests(req.user)
            .then(
                function (result) {
                    res.json(result);
                },

                function (err) {
                    console.log('error is ', err);
                    res.status(500).send(err);
                });
    },

    //USERS INTERACTION
    followUser: function followUser(req, res) {
        UsersInteractionService.followUser(req.params.id, req.user, req)
            .then(
                function (isFollowing) {
                    return res.json(isFollowing);
                },

                function (err) {
                    console.log('error ', err);
                    return res.status(500).send(err);
                }
            );
    },

    acceptRequest: function (req, res) {
        UsersInteractionService.acceptRequest(req.params.id, req.user)
            .then(
                function () {
                    res.status(200).send();
                },

                function (err) {
                    res.status(500).send(err);
                }
            );
    },

    declineRequest: function (req, res) {
        UsersInteractionService.declineRequest(req.params.id, req.user)
            .then(
                function () {
                    res.status(200).send();
                },

                function (err) {
                    res.status(500).send(err);
                }
            );
    },

    blockUser: function (req, res) {
        UsersInteractionService.blockUser(req.params.id, req.user)
            .then(
                function (isBlocked) {
                    res.json(isBlocked);
                },

                function (err) {
                    res.status(500).send(err);
                }
            );
    },

    getUserBadges: function (req, res) {
        UserService.getUsers(req.params.id)
            .then((user) => {
                BadgeService.getUserBadges(user, req.body)
                    .then(function (badges) {
                        return res.status(200).send(badges);
                    },

                    function (err) {
                        return res.status(500).
                            send(lang.getStringFromLang(req.query.lang, 'internal'));
                    });
            },

            function (err) {
                errHelper.getErrorDataFromCode(err, req.query.lang, 'findPremium')
                .then(function (err) {
                    return res.status(err.code).send(err.message);
                });
            });
    },

    getUserRanking: function (req, res) {
        UserService.getRanking(req.body)
            .then(function (users) {
                return res.status(200).send(users);
            },

            function (err) {
                return res.status(500).
                    send(lang.getStringFromLang(req.query.lang, 'internal'));
            });
    },

    toggleNotifications: function toggleNotifications(req, res) {
        AppUserAccountService.toggleNotifications(req.user)
            .then(
                function (values) {
                  return res.status(200).send(values);
                },

                function (err) {
                    return res.status(500).send(err);
                }
            );
    },

    getUserEvents: function getUserEvents(req, res) {
      AppUserService.getUserEvents(req.params.id, req.params.type, req.body)
          .then(function (events) {
              return res.status(200).send(events);
          },

          function (err) {
              return res.status(500).send(err);
          });
    }
};
