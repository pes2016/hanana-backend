'use strict';
var GroupService = require('../services/GroupService');
var EventService = require('../services/EventService');
var BadgeService = require('../services/BadgeService');
const _ = require('lodash');
var lang = require('../handlers/language-handler');
var errHelper = require('../helpers/error-helper');
var NotificationService = require('../services/NotificationService');

module.exports = {

    createGroup: function (req, res) {
        if (!req.file) {
            return res.status(400).send(lang.getStringFromLang(req.query.lang, 'nofile'));
        }

        if (!req.body.name || !req.body.description) {
            return res.status(400).send(lang.getStringFromLang(req.query.lang, 'fillAllFields'));
        }

        req.body.image = req.file.path;

        GroupService.createGroup(req.body, req.user)
             .then(function (id) {
                 return res.status(200).send(id);
             },

             function (err) {
                 errHelper.getErrorDataFromCode(err, req.query.lang, 'findGroup')
                 .then(function (err) {
                     return res.status(err.code).send(err.message);
                 });
             });

    },

    getOwnedGroups: function (req, res) {
        GroupService.getGroups(req.user.ownedGroups)
            .then(function (objs) {
                return res.status(200).send(objs);
            },

            function (err) {
                errHelper.getErrorDataFromCode(err, req.query.lang, 'findGroup')
                .then(function (err) {
                    return res.status(err.code).send(err.message);
                });
            });

        },

    getOwnedGroup: function (req, res) {
        if (req.user.ownedGroups.indexOf(req.params.id) === -1) {
            return res.status(400).send(lang.getStringFromLang(req.query.lang, 'findGroup', 2));
        }

        GroupService.getGroups(req.params.id)
            .then(function (obj) {
                return res.status(200).send(obj);
            },

            function (err) {
                errHelper.getErrorDataFromCode(err, req.query.lang, 'findGroup')
                .then(function (err) {
                    return res.status(err.code).send(err.message);
                });
            });
    },

    modifyGroup: function (req, res) {
        GroupService.modifyGroup(req.params.id, req.body, req.file)
            .then(function () {
                return res.status(200).send();
            },

            function (err) {
                errHelper.getErrorDataFromCode(err, req.query.lang, 'findGroup')
                .then(function (err) {
                    return res.status(err.code).send(err.message);
                });
            });
    },

    removeGroup: function (req, res) {
        GroupService.removeGroups(req.params.id)
            .then(function (removed) {
                return res.status(200).send(removed);
            },

            function (err) {
                errHelper.getErrorDataFromCode(err, req.query.lang, 'findGroup')
                .then(function (err) {
                    return res.status(err.code).send(err.message);
                });
            });
    },

    createEvent: function (req, res) {
        var userId = req.user.id;
        var data = req.body;

        if (!data || !data.title || data.title.trim() === '' || !data.description || !data.url ||
            data.description.trim() === '' || !data.date || data.date.trim() === '' ||
            !data.tags || data.tags.trim() === '' || !data.group || data.group.trim() === '' ||
            !req.file || !req.file.path || !parseFloat(data.lat, 10) ||
            !parseFloat(data.lon, 10) || !data.price || !data.age) {
                return res.status(400)
                    .send(lang.getStringFromLang(req.query.lang, 'fillAllFields'));
        }

        data.image = req.file.path;
        data.lat = parseFloat(data.lat, 10);
        data.lon = parseFloat(data.lon, 10);
        data.tags = data.tags.replace(' ', '').split(',');

        data.interests = data.interests || [];
        _.remove(data.interests, function (o) {return !o.isSelected;});

        console.log('Interests ', data.interests);

        EventService.createEvent(data)
            .then(function (event) {
                return res.status(200).send(event);
            },

            function (err) {
                errHelper.getErrorDataFromCode(err, req.query.lang, 'findEvent')
                .then(function (err) {
                    return res.status(err.code).send(err.message);
                });
            });

    },

    updateEvent: function (req, res) {
        var userId = req.user.id;
        var data = req.body;

        if (!data || !req.params.id) {
            return res.status(400).send(lang.getStringFromLang(req.query.lang, 'fillAllFields'));
        }

        data.tags = data.tags.replace(' ', '').split(',');

        EventService.modifyEvent(req.params.id, data, req.file)
            .then(function () {
                return res.status(200).send();
            },

            function (err) {
                errHelper.getErrorDataFromCode(err, req.query.lang, 'findEvent')
                .then(function (err) {
                    return res.status(err.code).send(err.message);
                });
            });

    },

    getQR: function (req, res) {
        EventService.getQR(req.params.id, req.user._id)
            .then(function (qrCode) {
                return res.status(200).send(qrCode);
            },

            function (err) {
                errHelper.getErrorDataFromCode(err, req.query.lang, 'findEvent')
                .then(function (err) {
                    return res.status(err.code).send(err.message);
                });
            });

    },

    getOwnedEvents: function (req, res) {
        EventService.getEventsFromGroups(req.user.ownedGroups)
            .then(function (objs) {
                return res.status(200).send(objs);
            },

            function (err) {
                errHelper.getErrorDataFromCode(err, req.query.lang, 'findEvent')
                .then(function (err) {
                    return res.status(err.code).send(err.message);
                });
            });
    },

    getOwnedEvent: function (req, res) {
        EventService.getEvents(req.params.id)
            .then(function (objs) {
                return res.status(200).send(objs);
            },

            function (err) {
                errHelper.getErrorDataFromCode(err, req.query.lang, 'findEvent')
                .then(function (err) {
                    return res.status(err.code).send(err.message);
                });
            });
    },

    removeEvent: function (req, res)  {
        EventService.removeEvents(req.params.id)
            .then(function (removed) {
                return res.status(200).send(removed);
            },

            function (err) {
                errHelper.getErrorDataFromCode(err, req.query.lang, 'findEvent')
                .then(function (err) {
                    return res.status(err.code).send(err.message);
                });
            });
    },

    getBadges: function (req, res) {
        BadgeService.getBadges(req.user, req.query.lang).then(function (badges) {
            return res.status(200).send(badges);
        },

        function (err) {
            errHelper.getErrorDataFromCode(err, req.query.lang, 'findBadge')
            .then(function (err) {
                return res.status(err.code).send(err.message);
            });
        });
    },

    createBadge: function (req, res) {

        if (!req.file) {
            return res.status(400).send(lang.getStringFromLang(req.query.lang, 'nofile'));
        }

        var data = req.body;

        if (!data.description || !data.name ||  !req.file.path ||
            !data.points || !data.trigger || !data.triggerValue ||
            !parseInt(data.triggerValue, 10) || !parseInt(data.trigger, 10) ||
             !parseInt(data.points, 10) || (!data.event && !data.group)) {
                 return res.status(400).send(lang.getStringFromLang(req.query.lang,
                      'fillAllFields'));
        }

        var badge = Object.assign(data, {
            image: req.file.path,
            owner: req.user._id,
            triggerArg: (data.event) ? data.event._id : data.group._id
        });

        BadgeService.createBadge(badge, req.user._id).then(function (badge) {
            return res.status(200).send(badge);
        },

        function (err) {
            errHelper.getErrorDataFromCode(err, req.query.lang, 'createBadge')
            .then(function (err) {
                return res.status(err.code).send(err.message);
            });
        });
    },

    getTriggers: function (req, res) {
        BadgeService.getTriggers(req.user, req.query.lang).then(function (triggers) {
            return res.status(200).send(triggers);
        });
    },

    notifyUsers: function (req, res) {
        NotificationService.notifyAll(req.body).then(
            function (values) {
                res.json(values);
            },

            function (error) {
                res.status(500).send(error);
            }
        );
    }
};
