'use strict'
const ComplaintService = require('../services/ComplaintService');
var lang = require('../handlers/language-handler');

module.exports = {

        reportUser: function (req, res) {
            var data = { author: req.user._id, reason: req.body.comment };
            ComplaintService.createComplaint(req.params.id, 'user', data)
                .then(function (id) {
                    return res.status(200).send(id);
                },

                function (err) {
                    return res.status(500).send(lang.getStringFromLang(req.query.lang, 'internal'));
                });
        },

        reportGroup: function (req, res) {
            var data = { author: req.user._id, reason: req.body.comment };
            ComplaintService.createComplaint(req.params.id, 'group', data)
                .then(function (id) {
                    return res.status(200).send(id);
                },

                function (err) {
                    return res.status(500).send(lang.getStringFromLang(req.query.lang, 'internal'));
                });
        },

        reportEvent: function (req, res) {
            var data = { author: req.user._id, reason: req.body.comment };
            ComplaintService.createComplaint(req.params.id, 'event', data)
                .then(function (id) {
                    return res.status(200).send(id);
                },

                function (err) {
                    return res.status(500).send(lang.getStringFromLang(req.query.lang, 'internal'));
                });
        }
};
