'use strict';
var langs = {
    en: require('../locales/en.js'),
    es: require('../locales/es.js'),
};

module.exports = {
      getStringFromLang: function (lang, key, code) {
          if (!lang) {
              lang = 'en';
          }

          if (!code && code !== 0) {
              return langs[lang][key];
          }

          return langs[lang][key][code];
      }
};
