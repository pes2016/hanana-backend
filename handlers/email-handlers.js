'use strict'
var config = require('../config.js');
var sendgrid = require('sendgrid')(config.apiKeyMailer);

let email = {
        sendNewPassword(email, name, pass) {
            return new Promise(function (resolve, reject) {
                var mailStruct = new sendgrid.Email({
                  to: email,
                  from: 'no-reply@hanana.com',
                  subject: 'Hanana\'s new password',
                  html: '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"' +
                          '"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">' +
                          '<html xmlns="http://www.w3.org/1999/xhtml">' +
                          '<body>' +
                          `<p>Hey ` + name + `,</p>` +
                          '<p>You requested a new premium\'s user password.</p>' +
                          `<p>Your new password is: <strong>` + pass + `</strong>.</p>` +
                          '<br/>' +
                          '<p>All the best.</p>' +
                          '<p> Hanana. </p>' +
                          '</body>' +
                          '</html>',
                });

                sendgrid.send(mailStruct, function (err, json) {
                    if (err) {
                        console.log(err);
                        return reject(5);
                    }

                    if (process.env.NODE_ENV !== 'production') {
                        console.log(json);
                    }

                    return resolve();
                });
            });
        }
    };

module.exports = email;
