'use strict'
var passport = require('passport');
var bcrypt = require('bcrypt-nodejs');

var _passport = require('../passport/passport');
_passport(passport);

var User = require('../models/user.js');
var validations = require('../helpers/validations');

var request = require('request');
var jwt = require('jsonwebtoken');
var uuid = require('uuid');
var config = require('../config.js');
const Utils = require('../helpers/utils');

function facebookSignUp(req, res) {
    if (!req.body || !req.body.access_token || !req.body.id) {
        return res.status(400).send('MAS PARAMETROS');
    }

    var token = req.body.access_token;
    var userId = req.body.id;
    var provider = req.params.provider;

    User.findOne({
        id: userId,
        provider: provider
    }, (err, user) => {
        if (err) {
            return res.status(400).send('Peto al buscar el user');
        }

        var url = 'https://graph.facebook.com';
        var path = '/me?fields=id,name,email,about,picture.width(800).height(800)&access_token=';
        request(url + path + token,
            (err, response, body) => {
                body = JSON.parse(body);
                if (err || response.status > 400 || userId !== body.id) {
                    return res.status(403).send('Unauthorised facebook vs body');
                }

                if (!user) {
                    user = Object.assign({}, body);
                    user.provider = provider;
                    user = new User(user);
                    user.isFirstLogin = true;
                } else {
                    user.isFirstLogin = false;
                }

                if (!user.avatar) {
                    user.avatar = body.picture.data.url;
                }

                if (!user.isActive) {
                  user.isActive = true;
                  user.isFirstLogin = true;
                }

                user.access_token = '';

                var access_token = jwt.sign({
                    id: user.id,
                    proider: provider
                }, config.secret, {
                    expiresIn: config.expiresTime // expires in 24 hours
                });
                var refresh_token = uuid.v1();

                user.access_token = access_token;
                user.refresh_token = refresh_token;

                user.save((err, doc) => {
                    if (err) {
                        return res.status(400).send(err);
                    }

                    return res.json(doc);
                });
            });
    });
}

function webSignUp(req, res) {
    let name = req.body.name;
    let email = req.body.email;
    let password = req.body.password;
    let provider = req.params.provider;

    if (name.trim().length === 0) {
        return res.status(400).send('Name cannot be empty');
    }

    if (!validations.emailValidation(email)) {
        return res.status(400)
            .send('Email cannot be empty & should be valid format');
    }

    if (password.trim().length < 6) {
        return res.status(400).send('Password should be at least 6 characters');
    }

    let hashedPassword = null;

    User.findOne({
        email: email
    }, (err0, user) => {
        if (err0) {
            return res.status(400).send('Error on finding user');
        }

        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(password, salt, null, (err1, hash) => {
                if (err1) {
                    return res.status(400).send('Error on hashing password');
                }

                hashedPassword = hash;

                // User object to be saved in MongoDB
                let user = new User({
                    name: name,
                    email: email,
                    password: hashedPassword,
                    provider: provider
                });

                var access_token = jwt.sign({
                    email: user.email,
                    provider: provider
                }, config.secret, {
                    expiresIn: config.expiresTime
                });

                var refresh_token = uuid.v1();
                user.access_token = access_token;
                user.refresh_token = refresh_token;

                if (!user.isActive) {
                  user.isActive = true;
                }

                user.save((err2, theUser) => {
                    if (err) {
                        return res.status(500).send('Could not save user');
                    }

                    return res.json({
                        access_token: user.access_token,
                        refresh_token: user.refresh_token
                    });
                });
            });
        });
    });
}

function twitterSignUp(req, res) {
    var options = {
        url: 'https://api.twitter.com/1.1/account/verify_credentials.json',
        headers: {
            Authorization: req.headers['x-verify-credentials-authorization']
        }
    };

    var provider = req.params.provider;

    // Make the request
    request(options, function (error, response, profile) {
        if (!error && response.statusCode === 200) {
            profile = JSON.parse(profile);
            profile.profile_image_url = profile.profile_image_url.replace('_normal', '');
            User.findOne({
                id: profile.id_str,
                provider: provider
            }, (err, user) => {
                if (!user) {
                    user = new User({
                        id: profile.id,
                        name: profile.name,
                        avatar: profile.profile_image_url,
                        about: profile.description,
                        provider: provider,
                    });
                    user.isFirstLogin = true;
                } else {
                  user.isFirstLogin = false;
                }

                if (!user.isActive) {
                  user.isActive = true;
                  user.isFirstLogin = true;
                }

                var access_token = jwt.sign({
                    id: user.id,
                    provider: user.provider
                }, config.secret, {
                    expiresIn: config.expiresTime
                });

                var refresh_token = uuid.v1();
                user.access_token = access_token;
                user.refresh_token = refresh_token;

                user.save((err, doc) => {
                    if (err) {
                        console.log('error savign token ', err);
                        return res.status(400).send('Error');
                    }

                    return res.json(doc);
                });
            });
        }else {
          console.log('error ', error);
          return res.status(400).send('Unauthorised');
        }
    });
}

let auth = {
    isAllowedByUser(req, res, next) {
        if (req.user._id.equals(req.params.id)) {
            console.log('its myself');
            return next();
        }

        User.findById(req.params.id).lean().exec((err, doc) => {
            if (err) {
                console.log('error is allowed ', err);
            }

            console.log('not error, doc',  doc);

            let isFollowing = Utils.isIdInArray(req.user._id, doc.followers);
            let isBlocked = Utils.isIdInArray(req.user._id, doc.blocked);

            console.log('is blocked ', isBlocked);
            console.log('is following  ', isFollowing);

            if (isBlocked || doc.isPrivate && !doc.following) {
                console.log('is blocked ');
                return res.send(404);
            }else {
                return next();
            }
        });
    },

    checkIfAdmin(req, res, next) {
        if (req.user && req.user.isAdmin) {
            next();
        } else {
            return res.status(401).send('Unauthorised');
        }
    },

    checkIfPremium(req, res, next) {
        if (req.user && req.user.isPremium) {
            next();
        } else {
            return res.status(401).send('Unauthorised');
        }
    },

    checkIfLoggedIn(req, res, next) {
        if (req.query.marcos) {
          return next();
        }

        jwt.verify(req.query.access_token, config.secret, function (err, decoded) {
            if (err) {
                console.log('Unauthorised ', err);
                return res.status(401).send('Unauthorised');
            } else {
                User.findOne({
                        access_token: req.query.access_token
                    },
                    (err, doc) => {
                        if (err) {
                            console.log('Error finding ', err);
                            return res.send(400);
                        }

                        if (!doc) {
                            return res.send(400);
                        }

                        console.log('Logged okai');
                        req.user = doc;
                        next();
                    });
            }
        });
    },

    register(req, res) {
        console.log('PROVIDER', req.params.provider);
        if (req.params.provider === 'facebook') {
            facebookSignUp(req, res);
        } else if (req.params.provider === 'twitter') {
            twitterSignUp(req, res);
        } else if (req.params.provider === 'web') {
            webSignUp(req, res);
        } else {
            return res.status(400).send();
        }
    },

    refreshToken(req, res) {
        if ((!req.body.id && !req.body.email) || !req.body.refresh_token) {
            return res.send(400);
        }

        var query = {
            refresh_token: req.body.refresh_token
        };

        if (req.body.id) {
            query.id = req.body.id;
        } else {
            query.email = req.body.email;
        }

        User.findOne(query, (err, user) => {
            if (err || !user) {
                return res.send(404);
            }

            var access_token = jwt.sign({}, config.secret, {
                expiresIn: config.expiresTime // expires in 24 hours
            });
            user.access_token = access_token;
            user.save((err, doc) => {
                if (err) {
                    return res.send(400);
                }

                res.json({
                    access_token: access_token
                });
            });
        });
    },

    /**
     * Handle user login process via Passport
     **/
    login(req, res) {
        let email = req.body.email;
        let password = req.body.password;

        if (!validations.emailValidation(email)) {
            return res.status(400).send('Please enter the correct email');
        }

        if (!password || password.trim().length < 6) {
            return res.status(400).send('Please enter the correct password');
        }

        passport.authenticate('local', (err0, user, info) => {
            if (err0) {
                return res.status(400).send('error', 'Cannot login. Try again.');
            }

            if (!user) {
                return res.status(400).send('User Not Found / Password is incorrect');
            }

            var access_token = jwt.sign({
                id: user.id,
                provider: 'web'
            }, config.secret, {
                expiresIn: config.expiresTime // expires in 24 hours
            });
            var refresh_token = uuid.v1();
            user.access_token = access_token;
            user.refresh_token = refresh_token;
            user.save(function (err) {
                if (err) {
                    return res.sendSatus(500);
                }

                return res.json({
                    premium: user.isPremium,
                    admin: user.isAdmin,
                    access_token: user.access_token,
                    refresh_token: user.refresh_token
                });
            });

        })(req, res);
    }
};

module.exports = auth;
