'use strict';

module.exports = {
      setPaging: function (model, params) {
        var page = params ? params.page || 0 : 0;
        if (params.page !== -1) {
          var limit = params ? params.limit || 50 : 50;
          model.skip(page * limit);
          model.limit(limit);
        }
      }
};
