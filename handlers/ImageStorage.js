'use strict'

var fs = require('fs');
var gm = require('gm');
var path = require('path');

function ImageStorage(opts) {
    if (!fs.existsSync(opts.directoryPath)) {
      fs.mkdirSync(opts.directoryPath);
    }

    this.getDestination = opts.destination;
    this.getFilename = opts.filename;
}

function formatToPNG(name) {
    console.log('format to png');
    var splittedFilename = name.replace(/ /g, '_').split('.');
    splittedFilename[splittedFilename.length - 1] = 'png';
    var filenameToStore = Date.now() + '_' + splittedFilename.join('.');
    return filenameToStore;
}

ImageStorage.prototype._handleFile = function _handleFile(req, file, cb) {
    var _this = this;
    console.log('_handlefile');
    this.getDestination(req, file, function (err, filePath) {
        if (err) {
            console.log('Error getting destination');
           return cb(err);
         }

        _this.getFilename(req, file, function (err, filename) {
            if (err) {
              return cb(err);
            }

            filePath = path.normalize(filePath + '/' + formatToPNG(filename));
            console.log('normalized file path : ', filePath);
            var outStream = fs.createWriteStream(filePath);
            var gmFileChain = gm(file.stream);
            gmFileChain.size({
                bufferStream: true
            }, function (err, size) {
                if (size && size.width >= 1024) {
                  this.resize(1024);
                }

                this.noProfile();
                this.trim();
                this.stream(function (err, stdout, stderr) {
                        if (err) {
                          console.log('ERROR ON STREAM ', err);
                        }

                        stdout.pipe(outStream);
                    });
            });

            outStream.on('error', function (err) {
              console.log('error ', err);
              cb();
            });

            outStream.on('finish', function () {
                console.log('Finished: ', filePath, ' and ', outStream.bytesWritten, ' BYTES');
                cb(null, {
                    path: filePath,
                    size: outStream.bytesWritten
                });
            });
        });
    });
};

ImageStorage.prototype._removeFile = function _removeFile(req, file, cb) {
    fs.unlink(file.path, cb);
};

module.exports = function (opts) {
    return new ImageStorage(opts);
};
