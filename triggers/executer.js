'use strict'

var Triggers = require('./triggers');
var Badge = require('./../models/badge');
var _ = require('lodash');

function loadTrigger(req, index, args) {
    var th;
    if (isNaN(index)) {

        // Accessing directly by this fnc
        th = Triggers.types[Triggers.triggers[index].type].handler;
        var id = Triggers.triggers[index].id;

        // Call the triggers method and pass the typeHandler CB
        return Triggers.triggers[index].method(req, th, id, args);
    } else {
        // Accessing by executeTriggers
        var Trigger = _.find(Triggers.triggers, function (trigger) {
            return trigger.id === index;
        });

        th = Triggers.types[Trigger.type].handler;

        // Call the triggers method and pass the typeHandler CB
        return Trigger.method(req, th, index, args);
    }
}

function returnCompleted(results) {
    return new Promise((res, rej) => {
        var completed = [];

        // Get the id of the completed badges
        _.each(results, (result) => {
            if (_.isArray(result)) {
                completed = completed.concat(_.map(result, 'badgeId'));
            }
        });

        // Get the actual completed badges
        Badge.find({ _id: { $in: completed } }).exec((err, badges) => {
            if (err) {
                console.log(err);
                return rej(err);
            }

            return res(badges);
        });
    });
}

function executeTriggers(req, kind, args) {
    return new Promise((res, rej) => {
        // Execute each trigger
        var proms = [];
        _.each(Triggers.triggers, (trigger) => {
            if (kind !== trigger.kind) {
                return;
            }

            proms.push(loadTrigger(req, trigger.id, args));
        });

        Promise.all(proms).then((results) => {
            // Get the object of the completed badges
            returnCompleted(results).then((completed) => {
                res(completed);
            }, (err) => {
                rej(err);
            });
        }, (err) => {
            rej(err);
        });
    });
}

module.exports = {
    triggerKinds: Triggers.triggerKinds,
    execute: executeTriggers,
    executeSingle: loadTrigger
};
