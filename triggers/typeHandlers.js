'use strict'

var Badge = require('../models/badge');
var User = require('../models/user');
var _ = require('lodash');

function countHandler(userBadges, badge, userBadge) {
    if (!userBadge) {
        userBadge = {
            badgeId: badge._id,
            currentValue: 0
        };
        userBadges.push(userBadge);
    } else {
        userBadge.currentValue = 0;
    }

    return userBadge;
}

function decreaseHandler(userBadges, badge, userBadge) {
    if (!userBadge) {
        userBadge = {
            badgeId: badge._id,
            currentValue: badge.triggerValue - 1
        };
        userBadges.push(userBadge);
    } else {
        userBadge.currentValue -= 1;
    }

    return userBadge;
}

function badgeReleaseHandler(req, badges, cb) {

    return new Promise((res, rej) => {

        var userBadges = req.user.badgeValues || [];
        var completedBadges = [];

        // For each badge update or insert it into the user data
        _.each(badges, (badge) => {
            var userBadge = _.find(userBadges, function (ub) {
                return badge._id.equals(ub.badgeId);
            });

            // Check if the badge is already completed
            var prevValue = (userBadge) ? userBadge.currentValue : badge.triggerValue;
            if (prevValue < 1) {
                return;
            }

            userBadge = cb(userBadges, badge, userBadge);

            // Mark has recently done so the APP can notice the user he has got a new badge
            if (userBadge.currentValue <= 0 && prevValue > 0) {
                completedBadges.push(userBadge);
                req.user.points += badge.points;
            }

        });

        // Make sure mongoose realises the array changed
        req.user.badgeValues = userBadges;

        // Update the user
        User.findOneAndUpdate({ _id: req.user._id }, req.user)
            .exec((err, doc) => {
            if (err) {
                console.log(err);
                return rej(err);
            }

            res(completedBadges);
        });
    });
}

module.exports = {
    decrease: function (req, badges) {
        return badgeReleaseHandler(req, badges, decreaseHandler);
    },

    count: function (req, badges) {
        return badgeReleaseHandler(req, badges, countHandler);
    }
};
