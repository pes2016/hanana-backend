'use strict'

var Badge = require('../models/badge');
var _ = require('lodash');

function checkBadges(badges, value) {
    var arr = [];
    _.each(badges, (badge) => {
        if (value >= badge.triggerValue) {
            arr.push(badge);
        }
    });
    return arr;
}

function attentEventHandler(req, badges, args) {
    // All the 'attent an event' badges will get decreased
    return new Promise((res, rej) => { res(badges); });
}

function completeProfileHandler(req, badges, args) {
    // There's just one 'complete whole profile' badge anyway
    return new Promise((res, rej) => { res(badges); });
}

function selectInterestsHandler(req, badges, args) {
    // There's just one 'select interests' badge anyway
    return new Promise((res, rej) => { res(badges); });
}

function createPrivateEventHandler(req, badges, args) {
    // There's just one 'create a private event' badge anyway
    return new Promise((res, rej) => { res(badges); });
}

function haveFriendsHandler(req, badges, args) {
    return new Promise((res, rej) => {
        req.user.getFriends().then((friends) => {
            // Check which badges have their requeriment completed
            var arr = checkBadges(badges, friends.length);

            // Return completed badges
            res(arr);
        }, () => {
            rej([]);
        });
    });

}

function followGroupsHandler(req, badges, args) {
    return new Promise((res, rej) => {
        // Check which badges have their requeriment completed
        var arr = checkBadges(badges, req.user.followingGroups.length);

        // Return completed badges
        res(arr);
    });
}

function haveBadgesHandler(req, badges, args) {
    return new Promise((res, rej) => {
        // Check which badges have their requeriment completed
        var userBadges = req.user.badgeValues;
        var cant = 0;
        _.each(userBadges, (ub) => {
            if (ub.currentValue <= 0) {
                cant++;
            }
        });

        // Check which badges get triggered
        var arr = checkBadges(badges, cant);

        // Return completed badges
        if (arr.length > 0) {
            res(arr);
        } else {
            rej();
        }
    });
}

function badgeFinder(req, typeHandler, trigger, badgeHandler, args) {

    return new Promise((res, rej) => {
        Badge.find({ trigger: trigger, isActive: true })
            .exec((err, badges) => {

            if (err) {
                console.log(err);
                rej(err);
            }

            if (!badges || !badges.length) {
                return res('No badges');
            }

            // Execute the handler and update the badges of the user
            badgeHandler(req, badges, args).then((triggeredBadges) => {
                typeHandler(req, triggeredBadges, args).then((cbs)=> {
                    res(cbs);
                }, (err) => {
                    rej(err);
                });
            });
        });
    });

}

module.exports = {
    // Completar el perfil
    completeProfile: function (req, typeHandler, trigger, args) {
        return badgeFinder(req, typeHandler, trigger, completeProfileHandler, args);
    },

    // Asistir a X Eventos
    attentEvent: function (req, typeHandler, trigger, args) {
        return badgeFinder(req, typeHandler, trigger, attentEventHandler, args);
    },

    // Tener amistad con X personas
    haveFriends: function (req, typeHandler, trigger, args) {
        return badgeFinder(req, typeHandler, trigger, haveFriendsHandler, args);
    },

    // Seguir a X grupos
    followGroups: function (req, typeHandler, trigger, args) {
        return badgeFinder(req, typeHandler, trigger, followGroupsHandler, args);
    },

    // Tener X badges
    haveBadges: function (req, typeHandler, trigger, args) {
        return badgeFinder(req, typeHandler, trigger, haveBadgesHandler, args);
    },

    // Escoger intereses
    selectInterests: function (req, typeHandler, trigger, args) {
        return badgeFinder(req, typeHandler, trigger, selectInterestsHandler, args);
    },

    // Crear un evento privado
    createPrivateEvent: function (req, typeHandler, trigger, args) {
        return badgeFinder(req, typeHandler, trigger, createPrivateEventHandler, args);
    }
};
