'use strict'

var typeHandlers = require('./typeHandlers');
var triggerHandlers = require('./triggerHandlers');
var premiumTriggerHandlers = require('./premiumHandlers');

// CONSTANTS for trigger kinds
const PROFILE = 0;
const ATTEND_EVENT = 1;
const FRIENDS = 2;
const FOLLOW_GROUP = 3;
const CREATE_PRIVATE = 4;
const OBTAIN_BADGE = 5;
const INTERESTS = 6;
const SHARE = 7;

module.exports.triggerKinds = {
    PROFILE: PROFILE,
    ATTEND_EVENT: ATTEND_EVENT,
    FRIENDS: FRIENDS,
    FOLLOW_GROUP: FOLLOW_GROUP,
    CREATE_PRIVATE: CREATE_PRIVATE,
    OBTAIN_BADGE: OBTAIN_BADGE,
    INTERESTS: INTERESTS,
    SHARE: SHARE
};

module.exports.triggers = {
    completeProfile: {
        id: 0,                                      // Id
        type: 0,                                    // Type Id
        premium: false,
        method: triggerHandlers.completeProfile,    // Method used for the typeHandler
        name: {
            es: 'Completar el perfil',
            en: 'Complete the profile'
        },
        kind: PROFILE
    },
    attentEvent: {
        id: 1,
        type: 0,
        premium: false,
        method: triggerHandlers.attentEvent,
        name: {
            es: 'Asistir a X Eventos',
            en: 'Assist X Events'
        },
        kind: ATTEND_EVENT
    },
    haveFriends: {
        id: 2,
        type: 1,
        premium: false,
        method: triggerHandlers.haveFriends,
        name: {
            es: 'Tener amistad con X personas',
            en: 'Be friends with X people'
        },
        kind: FRIENDS
    },
    followGroups: {
        id: 3,
        type: 1,
        premium: false,
        method: triggerHandlers.followGroups,
        name: {
            es: 'Seguir a X Grupos',
            en: 'Follow X Groups'
        },
        kind: FOLLOW_GROUP
    },
    haveBadges: {
        id: 4,
        type: 0,
        premium: false,
        method: triggerHandlers.haveBadges,
        name: {
            es: 'Obtener X Badges',
            en: 'Obtain X Badges'
        },
        kind: OBTAIN_BADGE
    },
    createPrivateEvent: {
        id: 5,
        type: 0,
        premium: false,
        method: triggerHandlers.createPrivateEvent,
        name: {
            es: 'Crear un evento privado',
            en: 'Create a private event'
        },
        kind: CREATE_PRIVATE
    },
    selectInterests: {
        id: 6,
        type: 0,
        premium: false,
        method: triggerHandlers.selectInterests,
        name: {
            es: 'Selecciona tus intereses',
            en: 'Select your interests'
        },
        kind: INTERESTS
    },
    attentAnEvent: {
        id: 7,
        type: 0,
        premium: true,
        method: premiumTriggerHandlers.attentAnEvent,
        name: {
            es: 'Asistir al evento X',
            en: 'Assist the event X'
        },
        kind: ATTEND_EVENT,
        arguments: 3

        // ownerId, groupId, eventId
    },
    attentGroupEvents: {
        id: 8,
        type: 0,
        premium: true,
        method: premiumTriggerHandlers.attentGroupEvents,
        name: {
            es: 'Asistir a X eventos del grupo Y',
            en: 'Assist to X events from group Y'
        },
        kind: ATTEND_EVENT,
        arguments: 2

        // ownerId, groupId
    },
    shareGroupEvents: {
        id: 9,
        type: 0,
        premium: true,
        method: premiumTriggerHandlers.shareGroupEvents,
        name: {
            es: 'Compartir X eventos del grupo Y',
            en: 'Share X events from group Y'
        },
        kind: SHARE,
        arguments: 2

        // ownerId, groupId
    },
    shareGroups: {
        id: 10,
        type: 0,
        premium: true,
        method: premiumTriggerHandlers.shareGroups,
        name: {
            es: 'Compartir X grupos',
            en: 'Share X groups'
        },
        kind: SHARE,
        arguments: 1

        // ownerId
    },
};

module.exports.types = [

    //  Type 0:
    //  - Action: Decrease counter each time the trigger activates
    //  - Completion condition: Counter reaches 0 or less

    {
        handler: typeHandlers.decrease
    },

    //  Type 1:
    //  - Action: Calculate counter each time the trigger activates
    //  - Completion condition: Counter reaches 0 or less

    {
        handler: typeHandlers.count
    }
];
