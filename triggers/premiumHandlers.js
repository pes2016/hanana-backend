'use strict'

var Badge = require('../models/badge');
var Event = require('../models/event');
var _ = require('lodash');

function checkBadges(badges, value) {
    var arr = [];
    _.each(badges, (badge) => {
        if (value >= badge.triggerValue) {
            arr.push(badge);
        }
    });
    return arr;
}

function checkBadgeArgs(badges, arg) {
    var arr = [];
    _.each(badges, (badge) => {
        if (arg === badge.triggerArg) {
            arr.push(badge);
        }
    });
    return arr;
}

function attentAnEventHandler(req, badges, args) {
    var eventId = args[2];
    return new Promise((res, rej) => {
        // Check which badges have their requeriment completed
        var arr = checkBadgeArgs(badges, eventId);

        // Return affected badges
        res(arr);
    });
}

function attentGroupEventsHandler(req, badges, args) {
    var groupId = args[1];
    return new Promise((res, rej) => {
        // Check which badges have their requeriment completed
        var arr = checkBadgeArgs(badges, groupId);

        // Return completed badges
        res(arr);
    });
}

function shareGroupEventsHandler(req, badges, args) {
    var groupId = args[1];
    return new Promise((res, rej) => {
        // Check which badges have their requeriment completed
        var arr = checkBadgeArgs(badges, groupId);

        // Return completed badges
        res(arr);
    });
}

function shareGroupsHandler(req, badges, args) {
    // All the 'share X owner's groups' badges will get decreased
    return new Promise((res, rej) => { res(badges); });
}

function badgeFinder(req, typeHandler, trigger, badgeHandler, args) {
    return new Promise((res, rej) => {
        Badge.find({ trigger: trigger, owner: args[0], isActive: true })
            .exec((err, badges) => {

            if (err) {
                console.log(err);
                return rej(err);
            }

            if (!badges || !badges.length) {
                return res('No badges');
            }

            // Execute the handler and update the badges of the user
            badgeHandler(req, badges, args).then((triggeredBadges) => {
                typeHandler(req, triggeredBadges, args).then((cbs)=> {
                    res(cbs);
                }, (err) => {
                    rej(err);
                });
            });
        });
    });

}

module.exports = {
    // Asistir al evento X
    attentAnEvent: function (req, typeHandler, trigger, args) {
        return badgeFinder(req, typeHandler, trigger, attentAnEventHandler, args);
    },

    // Asistir a X eventos del grupo Y
    attentGroupEvents: function (req, typeHandler, trigger, args) {
        return badgeFinder(req, typeHandler, trigger, attentGroupEventsHandler, args);
    },

    // Compartir X eventos del grupo Y
    shareGroupEvents: function (req, typeHandler, trigger, args) {
        return badgeFinder(req, typeHandler, trigger, shareGroupEventsHandler, args);
    },

    // Compartir X grupos
    shareGroups: function (req, typeHandler, trigger, args) {
        return badgeFinder(req, typeHandler, trigger, shareGroupsHandler, args);
    }
};
