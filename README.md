
## Hanana Backend


**How to install:**

- First of all, we need both node.js and mongoDB environments installed.
- Clone the git project and switch to desired branch.
- Run the install script `./install` if you want to install all dependencies and seed mongo. Keep in mind this will drop all previous data.
- Otherwise, run `npm install` and install frontend dependencies.

**How to start:**
- Make sure everything is installed properly.
- Use `npm start` to start the server with development environment.
- Use `NODE_ENV=production node server.js` to start the server with production environment.
- Use `node server.js` if `npm start` does not work properly.
- /app and /bower_components **must be in the same directory** (related to web project).

**Project execution order:**

0. install GraphicsMagick in order to compress images sent to the server:
  Ubuntu : apt-get install graphicsmagick
  Windows : https://sourceforge.net/projects/graphicsmagick/files/latest/download?source=files

1. Execute mongod.
2. Start the backend server.
3. Access to server with the corresponding app.

# Code style
    npm install -g jscs
    npm install -g jshint

  Atom linter : https://github.com/AtomLinter/linter-jshint

## Branch naming
    *{TYPEBRANCH}/{US###}-{USNAME}*

  **Type of Branch:**

   - feature
   - hotfix

## Commits

    *<TASKNUM>:Description*


## Seeding:
To run all seeds:

    npm run seeds
Single seeds:

	Users: 		node seeds/users/seed.js
	Events:		node seeds/events/seed.js
	Interests:  node seeds/interests/seed.js

## Check the code style
    jshint:     npm run linter
    jscs:       npm run checkStyle
    both:       npm run pretest

# Run tests
    Mocha:      mocha or ./node_modules/mocha/bin/mocha

# Full test coverage
                npm test

#### Using Express-Boilerplate

- [Node.js](https://nodejs.org/en/)
- [Expressjs](http://expressjs.com/en/index.html)
- [Mongoose](http://mongoosejs.com/)
- [Passport](http://passportjs.org/)
- [Mailgun](https://github.com/1lobby/mailgun-js)

##### Features
User authentication (User Registration, Login, Logout, Forgot Password)
