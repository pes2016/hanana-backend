'use strict';

module.exports = {
    selectByField: function (model, field, value) {
        var q = {};
        q[field] = {};
        q[field].$eq = value;
        model.and(q);
    },

    discardByField: function (model, field, value) {
        var q = {};
        q[field] = {};
        q[field].$ne = value;
        model.and(q);
    },

    selectValueIfInArray: function (model, field, array) {
        var q = {};
        q[field] = {};
        q[field].$in = array;
        model.and(q);
    },

    discardValueIfInArray: function (model, field, array) {
        var q = {};
        q[field] = {};
        q[field].$nin = array;
        model.and(q);
    },

    selectFutureDate: function (model) {
        model.and({
            date: {
                $gte: new Date()
            }
        });
    },

    selectPastDate: function (model) {
        model.and({
            date: {
                $lt: new Date()
            }
        });
    },

    sortBy: function (model, query) {
        model.sort(query);
    }
};
