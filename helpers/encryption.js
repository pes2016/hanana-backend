'use strict'

var bcrypt = require('bcrypt-nodejs');

module.exports = {

    generatePassword() {
        return new Promise(function (resolve, reject) {
            bcrypt.genSalt(10, (err, salt) => {
                if (err) {
                    console.log(err);
                    return reject(5);
                }

                var pass = newPassword();
                bcrypt.hash(pass, salt, null, (err1, hash) => {
                    if (err) {
                        console.log(err);
                        return reject(5);
                    }

                    return resolve({ unencrypted: pass, encrypted: hash });
                });
            });
        });
    }
};

function newPassword() {
    let newPass = '';
    let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for (let i = 0; i < 10; i++) {
        newPass += characters.charAt(Math.floor(Math.random() * characters.length));
    }

    return newPass;
}
