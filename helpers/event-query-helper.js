'use strict';
var QueryHelper = require('./queries');
var Utils = require('./utils');
var _ = require('lodash');

module.exports = {
    searchBy: function (model, params, or) {
        console.log('Search by : ', params);
        if (params && params.searchData) {
            var query = {};
            query.$or = [];
            query.$or.push({
                title: {
                    $regex: Utils.escape(params.searchData)
                }
            });
            query.$or.push({
                description: {
                    $regex: Utils.escape(params.searchData)
                }
            });
            query.$or.push({
                tags: {
                    $regex: Utils.escape(params.searchData)
                }
            });
            query.$or.push({
                interests: {
                    $regex: Utils.escape(params.searchData)
                }
            });
            console.log('Pushing find query ', query);
            if (or) {
                model.or(query);
            } else {
                model.and(query);
            }
        } else {
            console.log('No data to find');
        }
    },

    filterByInterests: function (model, interests) {
        console.log('Search by interests : ', interests);
        if (interests) {
            var query = {};
            query.$or = [];
            query.$or.push({
                interests: {
                    $in: interests
                }
            });
            console.log('Pushing find query ', query);
            model.and(query);
        } else {
            console.log('No data to find');
        }
    },

    filterByFollowing: function (model, ids) {
        console.log('Search by following : ', ids);
        if (ids) {
            var query = {};
            query.$or = [];
            query.$or.push({
                assistants: {
                    $in: ids
                }
            });
            console.log('Pushing find query ', query);
            model.and(query);
        } else {
            console.log('No data to find');
        }
    },

    selectPastEvents: function (model) {
        QueryHelper.selectPastDate(model);
    },

    selectFutureEvents: function (model) {
        QueryHelper.selectFutureDate(model);
    },

    selectByGroup: function (model, groupId) {
        QueryHelper.selectByField(model, 'group', groupId);
    },

    selectNearEvents: function (model, params) {
        let distance = params.distance || 5000;
        let lat = params.lat || 0;
        let lon = params.lon || 0;
        let radiusInRadians = parseFloat(distance / 2000 / 6378.1);
        var query = {
            location: {
                $geoWithin: {
                    $centerSphere: [
                        [parseFloat(lon), parseFloat(lat)], radiusInRadians
                    ]
                }
            }
        };
        model.and(query);
    },

    selectNearSphereEvents: function (model, params) {
        let distance = params.distance || 5000;
        let lat = params.lat || 0;
        let lon = params.lon || 0;
        let radiusInRadians = parseFloat(distance / 2000 / 6378.1);
        var query = {
            location: {
                    $nearSphere: [lon, lat],
                    $minDistance: 0,
                    $maxDistance: radiusInRadians
            }
        };
        model.and(query);
    },

    selectEventsForUser: function (model, userId) {
        QueryHelper.selectValueIfInArray(model, 'assistants', [userId]);
    },

    sortByDate: function (model, order) {
        order = order ? order : 1;
        QueryHelper.sortBy(model, {
            date: order
        });
    }
};
