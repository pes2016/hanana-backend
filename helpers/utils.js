'use strict'
var _ = require('lodash');
var User = require('../models/user');

RegExp.escape = function (str) {
    return String(str).replace(/([.*+?^=!:${}()|\[\]\/\\])/g, '\\$1');
};

function prepareRegexString(s, matchFromStart) {
    s = s || '';
    var escapedString = RegExp.escape(String(s));
    if (matchFromStart) {
      escapedString = '^' + escapedString;
    }

    var regexp = new RegExp(escapedString, 'i');
    return regexp;
}

function toggleArray(Model, field, id, value, callback) {
  Model.findById(id, (err, doc) => {
      if (err || !doc) {
          console.log('error ' + err);
      }

      doc[field] = doc[field] || [];

      var obj = _.find(doc[field], function (o) {
          return o.equals(value);
      });

      if (obj) {
          var pullQuery = { $pull: {} };
          pullQuery.$pull[field] = value;

          Model.update({
              _id: id
          },
          pullQuery,
           (err, data) => {
              if (err) {
                console.log('error ' + err);
              }

              return callback(false);
          });
      } else {
          doc[field].push(value);
          doc.save((err, doc) => {
              if (err) {
                  console.log('Error ', err);
              }

              return callback(true);
          });
      }
  });
}

function _idComparator(a, b) {
      return a === b;
}

function isIdInArray(id, array) {
    return Boolean(_.find(array, function (o) {
        return o.equals(id);
    }));
}

function isIdInArrayByField(id, array, field) {
    return Boolean(_.find(array, function (o) {
        return o[field].equals(id);
    }));
}

module.exports = {
  escape: prepareRegexString,
  toggleArray: toggleArray,
  _idComparator: _idComparator,
  isIdInArray: isIdInArray,
  isIdInArrayByField: isIdInArrayByField
};
