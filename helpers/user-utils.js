'use strict'
/**
 * Created by marcos on 03/12/2016.
 */
const Utils = require('../helpers/utils');
const _ = require('lodash');

function hasBlockedMe(user, requesterId) {
    return Utils.isIdInArrayByField(requesterId, user.blocked, '_id');
}

function removeUsersThatBlockedMe(list, requesterId) {
    //We remove users that blocked us
    _.remove(list, function (user) {
        return hasBlockedMe(user, requesterId);
    });
}

function removeUsersThatIHaveBlocked(list, user) {
    //We remove users that blocked us
    list = _.differenceWith(list, user.blocked, Utils._idComparator);
}

function calculateIfAlreadyFollowing(userList, userId) {
    _.map(userList, (user) => {
        user.isFollowing = Utils.isIdInArray(userId, user.followers);
    });
}

module.exports = {
    removeUsersThatBlockedMe: removeUsersThatBlockedMe,
    removeUsersThatIHaveBlocked: removeUsersThatIHaveBlocked,
    calculateIfAlreadyFollowing: calculateIfAlreadyFollowing,
};
