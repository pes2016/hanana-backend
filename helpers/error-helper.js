'use strict';
var langs = require('../handlers/language-handler');

function getHttpError(errCode) {
    if (errCode === 0) {
        return 400;
    } else if (errCode === 1) {
        return 404;
    } else if (errCode === 2) {
        return 401;
    } else if (errCode === 3) {
        return 400;
    } else if (errCode === 5) {
        return 500;
    }
}

module.exports = {
    getErrorDataFromCode: function (errCode, lang, funcName) {
        return new Promise(function (resolve, reject) {
            var err = {};
            err.code = getHttpError(errCode);
            if (errCode === 5) {
                err.message = langs.getStringFromLang(lang, 'internal');
            } else {
                err.message = langs.getStringFromLang(lang, funcName, errCode);
            }

            return resolve(err);
        });
    }
};
