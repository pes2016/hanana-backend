'use strict';
var QueryHelper = require('./queries');
var Utils = require('./utils');

const _ = require('lodash');

const USER_SEARCH_FIELDS = ['name', 'about'];

const APP_USER_FILTER = {
    isPremium: false,
    isAdmin: false,
    isActive: true,
};

module.exports = {
    discardSelf: function (model, value) {
        QueryHelper.discardByField(model, '_id', value);
    },

    discardUsersThatIHaveBlocked: function (model, array) {
        QueryHelper.discardValueIfInArray(model, '_id', array);
    },

    discardUsersThatBlockedMe: function (model, requesterId) {
        QueryHelper.discardValueIfInArray(model, 'blocked', [requesterId]);
    },

    searchBy: function (model, data) {
        if (data) {
            var orQuery = [];
            var regexData = { $regex: Utils.escape(data) };

            _.forEach(USER_SEARCH_FIELDS, (o) => {
                var query = {};
                query[o] = regexData;
                orQuery.push(query);
            });
            model.or(orQuery);
        }
    },

    projectUserListItem: function (model) {
        var projection = {
            name: 1,
            avatar: 1,
            followers: 1,
            following: 1,
            _id: 1
        };
        model.select(projection);
    },

    selectActiveAppUsers: function (model) {
        model.and(APP_USER_FILTER);
    }
};
