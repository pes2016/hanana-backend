/**
 * Created by marcos on 03/12/2016.
 */

'use strict';
var QueryHelper = require('./queries');
var Utils = require('./utils');

module.exports = {
    searchBy: function (model, params) {
        if (params.searchData) {
            var query = {};
            query.$or = [];
            query.$or = [];
            query.$or.push({
                name: {
                    $regex: Utils.escape(params.searchData)
                }
            });
            query.$or.push({
                description: {
                    $regex: Utils.escape(params.searchData)
                }
            });
            model.and(query);
        }
    }

};
