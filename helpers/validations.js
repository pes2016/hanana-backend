'use strict'
module.exports = {
    emailValidation(email) {
        if (!email || email === '') {
            return false;
        }

        let re = /\S+@\S+\.\S+/;
        let trimmedEmail = email.trim();
        if (!re.test(trimmedEmail) || trimmedEmail.length === 0) {
            return false;
        }

        return true;
    }
};
