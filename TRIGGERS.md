

# TRIGGERS

Collection amb una quantitat de triggers donada.

Cada trigger té un tipus i si és d'users premium o no.

### Activació

A les queries que poden accionar triggers se'ls posa un mètode al final de l'execució:

- Es busca segons el tipus de query quins triggers es podrien activar.
- Després amb cada trigger amb possible activació es busquen les badges amb el trigger. Si és un trigger premium, es filtra per id de premium id.
- Segons el trigger, s'aplica un mètode o un altre.
- Hi ha triggers que han de fer finds d'un atribut en comptes de decrementar directament el valor a l'array de badges.

Triggers.execute(req, Triggers.triggerKinds.ATTEND_EVENT,[ownerId, groupId]).then((completedBadges) => {

 res.status(200).send(completedBadges);

}, (err) => {

 res.status(400).send(err);

});
