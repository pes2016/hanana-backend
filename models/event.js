'use strict'
var mongoose = require('mongoose');

const Name = 'Event';
const Schema = new mongoose.Schema({

    id: String,

    title: String,

    description: String,

    date: {
        type: Date
    },

    lat: String,

    lon: String,

    address: String,

    tags: [],

    age: String,

    price: String,

    assistants: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }],
    image: {
        type: String,
        default: ''
    }, // TODO: ADD DEFAULT IMAGE

    qrCode: String,

    isActive: {
        type: Boolean,
        default: true
    },

    url: {
        type: String,
        default: ''
    },

    group: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Group'
    },
    attendees: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }],
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    isPrivate: {
        type: Boolean,
        default: false
    },
    rates: [
        {
            user: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
            },
            value: {
                type: Number,
                default: 1
            },
            comment: {
                type: String,
                default: ''
            },
            createdAt: {
                type: Date,
                default: Date.now()
            },
            upvotes: [{
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
            }],
            downvotes: [{
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User'
            }]
        }
    ],
    location: {},
    interests: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Interest'
    }]
});

Schema.pre('save', function (next) {
    if (this.lat && this.lon) {
        this.location = {
            coordinates: [Number(this.lat), Number(this.lon)],
            type: 'Point'
        };
    }

    next();
});

Schema.index({ location: '2dsphere' });

Schema.statics.markAsAttendee = function (eventId, userId, cb) {
    this.update({
            _id: eventId,
            date: {
                $lte: new Date()
            }
        }, {
            $addToSet: {
                attendees: userId,
                assistants: userId
            }
        },
        (err, doc) => {
            console.log('doc ', doc);
            cb(err, Boolean(doc.n));
        });
};

const Model = mongoose.model(Name, Schema);

module.exports = Model;
