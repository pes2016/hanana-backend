'use strict'
var mongoose = require('mongoose');
var fs = require('fs');
var Event = require('./event.js');

const Name = 'Group';
const Schema = new mongoose.Schema({

    id: String,

    name: {
        type: String,
        unique: true
    },

    description: String,

    image: String,

    isActive: {
        type: Boolean,
        default: true
    },

    owner: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },

    events: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Event',
    }],

    followers: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
      }
    ]
});

Schema.index({ name: 'text', description: 'text' });

const Model = mongoose.model(Name, Schema);

module.exports = Model;
