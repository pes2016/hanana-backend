'use strict'
var mongoose = require('mongoose');

const Name = 'Complaint';
const Schema = new mongoose.Schema({

    denounced: {
        type: mongoose.Schema.Types.ObjectId,
    },

    type: String,

    weight: {
        type: Number,
        default: 1
    },

    reports: [{
        _id: false,
        author: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        reason: String
    }]

});

const Model = mongoose.model(Name, Schema);

module.exports = Model;
