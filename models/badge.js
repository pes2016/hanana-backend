'use strict'
var mongoose = require('mongoose');

const Name = 'Badge';
const Schema = new mongoose.Schema({

    name: String,

    description: String,

    points: Number,

    image: {
        type: String,
        default: ''
    }, // TODO: ADD DEFAULT IMAGE

    isActive: {
        type: Boolean,
        default: true
    },

    owner: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },

    trigger: Number,

    triggerValue: Number,

    triggerArg: {}

});

const Model = mongoose.model(Name, Schema);

module.exports = Model;
