'use strict'
var mongoose = require('mongoose');

const Name = 'Interest';
const Schema = new mongoose.Schema({
  name: String,
  image: {
    type: String,
    default: ''
  }
});

const Model = mongoose.model(Name, Schema);

module.exports = Model;
