'use strict'
var mongoose = require('mongoose');

const BadgeValue = new mongoose.Schema({
    badgeId: String,
    currentValue: Number //Badge's Current progress. < 1 === done
});

const Name = 'User';
const Schema = new mongoose.Schema({

    // True if the user is premium. The other nonpremium fields will be nonexistent.
    isPremium: {
        type: Boolean,
        default: false
    },

    isAdmin: {
        type: Boolean,
        default: false
    },

    // Will exist if the user doesn't use facebook or twitter to identify himself
    // Just Premium users login with password, and just in the webpage
    password: {
        type: String
    },

    id: String,

    provider: {
        type: String,
        default: 'web'
    },

    //Will exist if the user uses facebook or twitter to identify himself
    access_token: {},

    //Used to generate a new access_token if it expires
    refresh_token: {},

    email: {
        type: String,
        unique: true
    },

    // User Info
    about: {
        type: String,
        default: ''
    },
    avatar: {
        type: String,
        default: ''
    },
    name: {
        type: String,
        default: ''
    },
    lastnames: {
        type: String,
        default: ''
    },

    // Materialized name + lastnames
    fullname: {
        type: String,
        default: ''
    },

    // Has the profile private?
    isPrivate: {
        type: Boolean,
        default: false
    },
    isActive: {
        type: Boolean,
        default: true
    },

    ownedGroups: {
        type: Array,
        default: []
    },

    interests: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Interest'
    }],

    isFirstLogin: {
      type: 'Boolean',
      default: true
    },

    followers: [
      {
          type: mongoose.Schema.Types.ObjectId,
          ref: 'User'
      }
    ],
    following: [
      {
          type: mongoose.Schema.Types.ObjectId,
          ref: 'User'
      }
    ],
    blocked: [
      {
          type: mongoose.Schema.Types.ObjectId,
          ref: 'User'
      }
    ],
    pending: [
      {
          type: mongoose.Schema.Types.ObjectId,
          ref: 'User'
      }
    ],
    discarded: [
      {
          type: mongoose.Schema.Types.ObjectId,
          ref: 'User'
      }
    ],
    followingGroups: [
      {
          type: mongoose.Schema.Types.ObjectId,
          ref: 'Group'
      }
    ],
    points: {
        type: Number,
        default: 0
    },

    badgeValues: [BadgeValue],
    pushNotifications: {
      type: Boolean,
      default: true
    }
});

Schema.methods.getFriends = function () {
    return new Promise((res, rej) => {
        Model.find({ _id: { $in: this.following }, following: this._id })
            .exec((err, friends) => {
            if (err) {
                console.log(err);
                rej(err);
            }

            res(friends);
        });
    });
};

Schema.statics.getFieldPopulatedForId = function (userId, field) {
    return new Promise((resolve, reject) => {
        Model.findById(userId).populate(field).lean()
            .exec((err, user) => {
                if (err) {
                    reject(err);
                }

                resolve(user[field]);
            });
    });
};

const Model = mongoose.model(Name, Schema);

module.exports = Model;
