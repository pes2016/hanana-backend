'use strict'

module.exports = {
    internal: 'Internal Error.',
    nofile: 'No image provided.',
    fillAllFields: 'Please fill correctly all the required fields.',
    fillOneField: 'Please fill atleast one field.',
    findPremium: {
        0: 'Already exists a user with that email.',
        1: 'User not found.'
    },
    registerPremiumUser: {
        email: 'Email is incorrect.',
    },
    findBadge: {
        1: 'Badge not found.',
        2: 'You don\'t own that badge.'
    },
    createBadge: {
        0: 'Not enough points available to create that badge.'
    },
    findComplaint: {
        1: 'Complaint not found.'
    },
    findGroup: {
        0: 'Already exists a group with that name.',
        1: 'Group not found.',
        2: 'You don\'t own that group.'
    },
    findEvent: {
        1: 'Event not found.',
        2: 'You don\'t own that event.',
        3: 'That event doesn\'t have a QR code.'
    }
};
