'use strict'

module.exports = {
    internal: 'Error interno.',
    nofile: 'No se ha proporcionado ninguna imagen.',
    fillAllFields: 'Por favor rellene correctamente todos los campos obligatorios.',
    fillOneField: 'Por favor rellene al menos un campo.',
    findPremium: {
        0: 'Ya existe un usuario con ese correo electrónico.',
        1: 'Usuario no encontrado.'
    },
    registerPremiumUser: {
        email: 'El correo electrónico es incorrecto.',
    },
    findBadge: {
        1: 'Insignia no encontrada.',
        2: 'Esa insignia no te pertenece.'
    },
    createBadge: {
        0: 'No tienes suficientes puntos disponibles para crear esa insignia.'
    },
    findComplaint: {
        1: 'Queja no encontrada.'
    },
    findGroup: {
        0: 'Ya existe un grupo con ese nombre.',
        1: 'Grupo no encontrado.',
        2: 'Ese grupo no te pertenece.'
    },
    findEvent: {
        1: 'Evento no encontrado.',
        2: 'Ese evento no te pertenece.',
        3: 'Ese evento no dispone de un código QR.'
    }
};
