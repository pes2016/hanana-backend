/**
 * Created by marcos on 03/12/2016.
 */

'use strict';
const User = require('../models/user');
const Interest = require('../models/interest');
const UserUtils = require('../helpers/user-utils');
const _ = require('lodash');
const Utils = require('../helpers/utils');
const UserQueryHelper = require('../helpers/user-query-helper');
const paging = require('../handlers/paging-handler');

const AppEventService = require('../services/AppEventService');
const as = require('async');

const APP_USER_FIELDS_TO_HIDE = {
    password: 0,
    interests: 0,
    access_token: 0,
    refresh_token: 0
};

function applyUserFilters(list, user) {
    UserUtils.removeUsersThatBlockedMe(list, user._id);
    UserUtils.removeUsersThatIHaveBlocked(list, user);
    UserUtils.calculateIfAlreadyFollowing(list, user._id);
}

module.exports = {
    getUserInterests: function (userId) {
        return new Promise(function (resolve, reject) {
            let promises = [
                User.findById(userId, {
                    interests: 1
                }).lean(),
                Interest.find().lean()
            ];
            Promise.all(promises)
                .then(function ok(values) {
                    let userInterests = values[0].interests;
                    let systemInterests = values[1];
                    _.map(systemInterests, (interest) => {
                        interest.isSelected = Utils.isIdInArray(interest._id, userInterests);
                        return interest;
                    });
                    return resolve(systemInterests);
                })
                .catch(function (err) {
                    return reject(err);
                });
        });
    },

    getUsers: function (params, me) {
        return new Promise(function (resolve, reject) {
            let query = User.find();
            UserQueryHelper.discardSelf(query, me._id);
            UserQueryHelper.discardUsersThatIHaveBlocked(query, me.blocked);
            UserQueryHelper.discardUsersThatBlockedMe(query, me._id);
            UserQueryHelper.selectActiveAppUsers(query);
            UserQueryHelper.searchBy(query, params.searchData);
            UserQueryHelper.projectUserListItem(query);
            paging.setPaging(query, params);
            query.lean();

            query.exec(function (err, docs) {
                if (err) {
                    console.log('Error executing query ', err);
                    return reject(err);
                }

                let myFollowing = me.following || [];

                //Get number of followers and followers in common
                _.map(docs, (o) => {
                    let userFollowers = o.followers || [];
                    o.numberOfFollowers = userFollowers.length;

                    let result = _.intersectionWith(myFollowing,
                        userFollowers, Utils._idComparator);
                    o.numberOfFollowersInCommon = result.length;
                    return o;
                });
                resolve(docs);
            });

        });
    },

    getUserProfile: function (userIdToFind, me, params) {
        return new Promise(function (resolve, reject) {
            User.findById(userIdToFind, APP_USER_FIELDS_TO_HIDE)
                .populate('followingGroups')
                .lean()
                .exec((err, user) => {
                    if (err) {
                        return reject(err);
                    }

                    user.blocked = user.blocked || [];
                    user.following = user.following || [];
                    user.followers = user.followers || [];
                    user.pending = user.pending || [];
                    user.discarded = user.discarded || [];
                    user.followingGroups = user.followingGroups || [];

                    user.isFollowing = Utils.isIdInArray(me._id, user.followers);
                    user.isPending = Utils.isIdInArray(me._id, user.pending);
                    user.isDiscarded = Utils.isIdInArray(me._id, user.discarded);
                    user.isFollowingBack = Utils.isIdInArray(user._id, me.followers);

                    user.canChat = (user.isFollowingBack && user.isFollowing);

                    user.numberOfFollowers = user.followers.length;
                    user.numberOfFollowings = user.following.length;
                    user.numberOfGroupsFollowed = user.followingGroups.length;

                    if ((user.isPrivate && !user.isFollowing &&
                            !me._id.equals(user._id)) || user.isPending) {
                        delete user.events;
                        delete user.followingGroups;
                        return resolve(user);
                    } else {
                        AppEventService.getEventsForUser(user, 'future', params)
                            .then(
                                function (docs) {
                                    user.events = docs;
                                    as.map(user.followingGroups,
                                        (o, cb) => {

                                            //Number of followers
                                            o.followers = o.followers || [];
                                            o.numberOfFollowers = o.followers.length;

                                            //Number of events
                                            AppEventService.getNumberOfEventsByGroup(o._id)
                                                .then(function (data) {
                                                    o.numberOfEvents = data;
                                                    return cb(null, o);
                                                }, function (err) {

                                                    return cb(err);
                                                });
                                        },

                                        (err, docs) => {
                                            if (err) {
                                                return reject(err);
                                            }

                                            return resolve(user);
                                        });
                                },

                                function (err) {
                                    return reject(err);
                                });
                    }
                });
        });
    },

    getBlockedUsers: function (user) {
        return new Promise(function (resolve, reject) {
            //We get the blocked array populated
            User.getFieldPopulatedForId(user._id, 'blocked')
                .then(
                    function (result) {
                        return resolve(result);
                    },

                    function (err) {

                        return reject(err);
                    }
                );
        });
    },

    getUserFollowers: function (userIdToFind, user) {
        return new Promise(function (resolve, reject) {
            //We get the blocked array populated
            User.getFieldPopulatedForId(userIdToFind, 'followers')
                .then(
                    function (followers) {
                        applyUserFilters(followers, user);
                        return resolve({
                            users: followers
                        });
                    },

                    function (err) {

                        return reject(err);
                    }
                );
        });
    },

    getUserFollowing: function (userIdToFind, user) {
        return new Promise(function (resolve, reject) {
            User.getFieldPopulatedForId(userIdToFind, 'following')
                .then(
                    function (following) {
                        console.log('before ', following);
                        applyUserFilters(following, user);
                        console.log('resolving ', following);
                        return resolve({
                            users: following
                        });
                    },

                    function (err) {

                        return reject(err);
                    }
                );
        });
    },

    getUserPendingRequests: function (user) {
        return new Promise(function (resolve, reject) {
            User.getFieldPopulatedForId(user._id, 'pending')
                .then(
                    function (pending) {
                        return resolve({
                            users: pending
                        });
                    },

                    function (err) {

                        return reject(err);
                    }
                );
        });
    },

    getUserEvents: function (userId, type, params) {
        return new Promise(function (resolve, reject) {
          User.findById(userId).exec(function (err, user) {
            if (err) {
              return reject(err);
            }

            AppEventService.getEventsForUser(user, type, params)
                .then(
                    function (events) {
                        return resolve({
                            events: events
                        });
                    },

                    function (err) {
                        return reject(err);
                    }
                );
        });
          });
    }
};
