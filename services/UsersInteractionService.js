/**
 * Created by marcos on 03/12/2016.
 */
'use strict'

const User = require('../models/user');
const _ = require('lodash');
const Utils = require('../helpers/utils');
const paging = require('../handlers/paging-handler');
const Triggers = require('../triggers/executer');

module.exports = {

    followUser: function (userId, self, req) {
        return new Promise(function (resolve, reject) {
            User.findById(userId)
                .exec((err, user) => {
                    if (err) {
                        reject(err);
                    }

                    user.followers = user.followers || [];
                    user.pending = user.pending || [];
                    self.following = self.following || [];

                    let alreadyFollowing = Utils.isIdInArray(self._id, user.followers);
                    let isPending = Utils.isIdInArray(self._id, user.pending);

                    if (alreadyFollowing) {
                        console.log('is already following so unfollow');
                        _.remove(user.followers, function (o) {
                            return o.equals(self._id);
                        });

                        _.remove(self.following, function (o) {
                            return o.equals(user._id);
                        });
                    } else if (isPending) {
                        console.log('Is pending');
                        _.remove(user.pending, function (o) {
                            return o.equals(self._id);
                        });
                    } else if (user.isPrivate) {
                        console.log('is already following so pending');
                        user.pending.push(self._id);
                    } else {
                        console.log('lets follow');
                        user.followers.push(self._id);
                        self.following.push(user._id);

                        //TODO #TRIGGER NO VA
                        Triggers.execute(req, Triggers.triggerKinds.FRIENDS, [])
                            .then((completedBadges) => {
                              console.log('Trigger executed okai');
                            }, (err) => {
                              console.log('Trigger executed wrong');
                            });

                    }

                    user.markModified('following');
                    user.markModified('followers');
                    user.markModified('pending');
                    user.save((err) => {
                        if (err) {
                            console.log('error saving user self: ', err);
                            return reject(err);
                        }
                    });

                    self.markModified('following');
                    self.markModified('followers');
                    console.log('Gonna save ', self);
                    self.save((err) => {
                        if (err) {
                            console.log('error saving user self: ', err);
                            return reject(err);
                        }
                    });
                    return resolve(!alreadyFollowing);
                });
        });
    },

    blockUser: function (userId, self) {
        return new Promise(function (resolve, reject) {
            User.findById(userId).exec((err, userToBlock) => {
                if (err) {
                    console.log('Error');
                    return reject(err);
                }

                let match = _.find(self.blocked, function (o) {
                    return o.equals(userId);
                });

                if (match) {
                    //The user is already blocked, so unblock;
                    _.remove(self.blocked, function (o) {
                        return o.equals(userId);
                    });
                } else {
                    //The user is not blocked, we do:
                    self.blocked[self.blocked.length] = userToBlock._id;
                }

                //Let's reset the follow flow:
                _.remove(self.followers, function (o) {
                    return o.equals(userId);
                });

                _.remove(self.following, function (o) {
                    return o.equals(userId);
                });

                _.remove(userToBlock.followers, function (o) {
                    return o.equals(userId);
                });

                _.remove(userToBlock.following, function (o) {
                    return o.equals(userId);
                });

                self.markModified('blocked');
                self.markModified('following');
                self.markModified('followers');
                userToBlock.markModified('following');
                userToBlock.markModified('followers');

                self.save((err) => {
                    if (err) {
                        console.log('Error saving user: ', err);
                        return reject(err);
                    }
                });
                userToBlock.save((err) => {
                    if (err) {
                        console.log('Error saving userToBlock: ', err);
                        return reject(err);
                    }
                });

                return resolve(Boolean(!match));
            });
        });
    },

    declineRequest: function (userIdToDecline, self) {
        return new Promise(function (resolve, reject) {
            User.findById(userIdToDecline).exec((err, doc) => {
                if (err) {
                    return reject(err);
                }

                _.remove(self.pending, doc._id);
                self.discarded = self.discarded || [];
                self.discarded.push(doc._id);
                self.markModified('pending');
                self.markModified('discarded');
                self.save(function (err) {
                    return reject(err);
                });

                return resolve();
            });
        });
    },

    acceptRequest: function (userIdToAccept, self) {
        return new Promise(function (resolve, reject) {
            User.findById(userIdToAccept).exec((err, doc) => {
                doc.following = doc.following || [];
                doc.following.push(self._id);
                doc.markModified('following');
                doc.save(function (err) {
                    return reject(err);
                });

                self.followers = self.followers || [];
                _.remove(self.pending, function (o) {
                    return o.equals(doc._id);
                });

                self.followers.push(doc._id);
                self.markModified('followers');
                self.markModified('pending');
                self.save(function (err) {
                    return reject(err);
                });

                return resolve();
            });
        });
    }

};
