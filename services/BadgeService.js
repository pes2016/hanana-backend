'use strict';
var Badge = require('../models/badge');
var Event = require('../models/event');
var GroupService = require('./GroupService');
var EventService = require('./EventService');
const Triggers = require('../triggers/triggers').triggers;
var triggerArray = Object.keys(Triggers);
var _ = require('lodash');
const paging = require('../handlers/paging-handler');

function pointsBought(userId, points) {
    // IF SUPER EXITOW AND REAL PRODUCTION
    // VALIDATE THAT POINTS HAVE REALLY BEEN BOUGHT
    return true;
}

function releasePoints(userId, points) {
    // IF SUPER EXITOW AND REAL PRODUCTION
    // MARK THE RECENTLY BOUGHT POINTS AS USED
    return true;
}

/**
* Creates a new badge.
* @param {Object} badge - {
*     {String} name - name of the badge,
*     {String} description - description of the badge,
*     {String} image - path to the image of the badge from rootdir
*     {Integer} points - points awarded by unlocking the badge
*     {Integer} trigger - trigger kind
*     {Integer} triggerValue - value of the trigger kind to unlock the badge
*     {ObjectID} owner - (optional) Group to whom the badge belongs.
* }
* @param {ObjectID} user - User that creates the badge.
* @return {Promise} if OK new badge's ID
*                   else {errCode}
*/
function createBadge(badge, user) {
    return new Promise(function (resolve, reject) {

        var newBadge = new Badge(badge);
        if (badge.owner) {
            if (!pointsBought(user, badge.points)) {
                return reject(0);
            }

        }

        newBadge.save((err) => {
            if (err) {
                console.log(err);
                return reject(5);
            }

            if (badge.owner) {
                releasePoints(user, badge.points);
            }

            return resolve(newBadge._id);
        });
    });
}

/**
* Modifies a badge.
* @param {ObjectID} badgeId - {
* @param {Object} badge - {
*     {String} name - name of the badge,
*     {String} description - description of the badge,
*     {String} image - path to the image of the badge from rootdir
*     {Integer} points - points awarded by unlocking the badge
*     {Integer} trigger - trigger kind
*     {Integer} triggerValue - value of the trigger kind to unlock the badge
*     {ObjectID} owner - (optional) Group to whom the badge belongs.
* }
* @param {ObjectID} user - User that edits the badge.
* @return {Promise} if OK  badge's ID
*                   else {errCode}
*/
function editBadge(badgeId, badge, userId) {
    return new Promise(function (resolve, reject) {
        Badge.findById(badgeId).exec((err, obj) => {
            if (err) {
                console.log(err);
                return reject(5);
            }

            if (!obj) {
                return reject(1);
            }

            if (userId.equals(obj.owner)) {
                return reject(2);
            }

            obj = Object.assign(obj, badge);

            obj.save((err, mod) => {
                if (err) {
                    console.log(err);
                    return reject(5);
                }

                return resolve(badge._id);
            });
        });
    });
}

function getTriggerStr(badge, lang, arg) {
    if (!lang) {
        lang = 'es';
    }

    var str = '';
    if (badge.trigger === 7) {
        str = Triggers[triggerArray[badge.trigger]].name[lang]
            .replace(/ X/, ' ' + arg.title);
    } else {
        str = Triggers[triggerArray[badge.trigger]].name[lang]
            .replace(/ X /, ' ' + badge.triggerValue + ' ');
        if (badge.trigger === 8) {
            str = str.replace(/ Y/, ' ' + arg.name);
        }
    }

    return str;
}

/**
* Retrieves all badges from a user. If admin gets all, if premium only created by him.
* @param {User} user - User to whom we want to get modifiable badges.
* @return {Promise} if OK [{Badges where isActive == true}] ordered by _id DESC
*                   else {errCode}
*/
function getBadges(user, lang) {
    return new Promise(function (resolve, reject) {
        var query;

        if (user.isPremium) {
            query = Badge.find({ isActive: true, owner: user._id })
                .lean().sort({ _id: -1 });
        } else if (user.isAdmin) {
            query = Badge.find({ isActive: true }).lean();
        }

        query.exec(function (err, objs) {
            if (err) {
                return reject(5);
            }

            var proms = [];
            objs.forEach(function (item, index) {
                proms.push(new Promise((resolve, reject) => {
                    if (item.trigger === 7) { // Lookup the event
                        EventService.getEvents(item.triggerArg)
                            .then((event) => {
                                item.triggerStr = getTriggerStr(item, lang, event);
                                resolve();
                            }, (err) => {
                                reject();
                            });
                    } else if (item.trigger === 8) { // Lookup the group
                        GroupService.getGroups(item.triggerArg)
                            .then((group) => {
                                item.triggerStr = getTriggerStr(item, lang, group);
                                resolve();
                            }, (err) => {
                                reject();
                            });
                    } else {
                        item.triggerStr = getTriggerStr(item, lang);
                        resolve();
                    }
                }));
            });

            Promise.all(proms).then(() => {
                resolve(objs);
            }, (err) => {
                reject(5);
            });
        });
    });
}

/**
* Retrieves a badge from a given id if the given user owns it.
* @param {User} user - User to whom we want to get modifiable badges.
* @param {ObjectID} badgeId - Badge Id
* @return {Promise} if OK [{Badges where isActive == true}] ordered by _id DESC
*                   else {errCode}
*/
function getBadge(user, badgeId, lang) {
    return new Promise(function (resolve, reject) {
        if (!lang) {
            lang = 'es';
        }

        var query;
        if (user.isPremium) {
            query = Badge.findOne({ isActive: true, _id: badgeId,
                owner: { $in: user.ownedGroups }
            }).populate('owner', 'name').lean().sort({ _id: -1 });
        } else if (user.isAdmin) {
            query = Badge.findOne({ _id: badgeId, isActive: true }).
                populate('owner', 'name').lean();
        }

        query.exec(function (err, badge) {
            if (err) {
                return reject(5);
            }

            if (!badge) {
                return reject(1);
            }

            badge.triggerStr = Triggers[triggerArray[badge.trigger]]
                .name[lang].replace(/ X /, ' ' + badge.triggerValue + ' ');
            if (badge.owner) {
                badge.triggerStr = badge.triggerStr.replace(/ Y/, ' ' + badge.owner.name);
            }

            resolve(badge);
        });
    });
}

/**
* Retrieves the current badges from a user.
* @param {User} user - User to whom we want to get modifiable badges.
* @return {Promise} if no error -> {badges : [badges]}
*                   else {errCode}
*/
function getUserBadges(user, params, lang) {
    return new Promise(function (resolve, reject) {
        if (!lang) {
            lang = 'es';
        }

        var query;
        var ids = [];
        var userBadges = {};

        _.each(user.badgeValues, (badge) => {
            ids.push(badge.badgeId);
            userBadges[badge.badgeId] = {
                currentValue: badge.currentValue
            };
        });
        var BadgeQuery = Badge.find({ _id: { $in: ids } });
        paging.setPaging(BadgeQuery, params);
        BadgeQuery.exec(function (err, badges) {
            if (err) {
                console.log(err);
                return reject(5);
            }

            var result = [];

            _.each(badges, (badge) => {
                var id = badge._id.toString();

                var triggerStr = Triggers[triggerArray[badge.trigger]]
                    .name[lang].replace(/ X /, ' ' + badge.triggerValue + ' ');
                if (badge.owner) {
                    triggerStr = triggerStr.replace(/ Y/, ' ' + badge.owner.name);
                }

                result[result.length] = Object.assign({}, userBadges[id], {
                    _id: badge._id,
                    name: badge.name,
                    description: badge.description,
                    points: badge.points,
                    image: badge.image,
                    isActive: badge.isActive,
                    triggerStr: triggerStr,
                    trigger: badge.trigger,
                    triggerValue: badge.triggerValue,
                    triggerArg: badge.triggerArg
                });
            });

            resolve({ badges: result });
        });
    });
}

/**
* Retrieves all triggers available to a user.
* @param {User} user - User who wants to get triggers.
* @return {Promise} if OK [triggers available to user]
*                   else {errCode}
*/
function getTriggers(user, lang) {
    return new Promise(function (resolve, reject) {
        if (!lang) {
            lang = 'es';
        }

        var result = [];
        for (var trigger in Triggers) {
            if (Triggers.hasOwnProperty(trigger)) {
                if (user.isPremium === Triggers[trigger].premium) {
                    var aux = {
                        id: Triggers[trigger].id,
                        type: Triggers[trigger].type,
                        premium: Triggers[trigger].premium,
                        method: Triggers[trigger].method,
                        name: Triggers[trigger].name[lang],
                        kind: Triggers[trigger].kind,
                        arguments: Triggers[trigger].arguments
                    };
                    result.push(aux);
                }
            }
        }

        if (result.length === 0) {
            return reject(1);
        }

        return resolve(result);
    });
}

module.exports = {
    createBadge: createBadge,
    getBadges: getBadges,
    getTriggers: getTriggers,
    getBadge: getBadge,
    editBadge: editBadge,
    getUserBadges: getUserBadges
};
