'use strict';
var Complaint = require('../models/complaint');
var paging = require('../handlers/paging-handler');
var UserService = require('./UserService');
var GroupService = require('./GroupService');
var EventService = require('./EventService');

var getQuery = {
     user: function (id) {
         return UserService.getUsers(id, { select: 'email fullname about avatar isActive' });
     },

     group: function (id) {
         return GroupService.getGroups(id, { select: 'name description image isActive' });
     },

     event: function (id) {
         return EventService.getEvents(id, { select: 'title description image url isActive' });
     }
 };

 var deleteQuery = {
      user: function (id) {
          return UserService.removeUser(id);
      },

      group: function (id) {
          return GroupService.removeGroups(id);
      },

      event: function (id) {
          return EventService.removeEvents(id);
      }
  };

 /**
 * Creates a new complaint.
 * @param {ObjectId} id - Id of reported item.
 * @param {String} type - Type of reported item. Can be: user, group or event.
 * @param {Object} data - {
 *     {ObjectId} author - Id of the author of the report.
 *     {String} reason - reason of the report.
 * }
 * @return {Promise} if OK complaint's ID
 *                   else {errCode, errMessage}
 */
 function createComplaint(id, type, data) {
     return new Promise(function (resolve, reject) {
         Complaint.findOne({ denounced: id }, function (err, complaint) {
             if (err) {
                 console.log(err);
                 return reject(5);
             }

             if (complaint) {
                 complaint.reports.push(data);
                 ++complaint.weight;
                 complaint.save(function (err) {
                     if (err) {
                         console.log(err);
                         return reject(5);
                     }

                     return resolve(complaint._id);
                 });
             } else {

                 var newComplaint = new Complaint({
                     denounced: id,
                     type: type,
                     reports: [data]
                 });

                 newComplaint.save(function (err, complaint) {
                     if (err) {
                         console.log(err);
                         return reject(5);
                     }

                     return resolve(complaint._id);
                 });
             }

         });
     });
 }

 /**
 * Gets a complaint. Complaints fill an entire page. There's support for pagination.
 * Because denounced is a ref, if denouced is not found
 * complaint is removed and function is recalled.
 * @param {Number} page - Number of item on pending complaint's list.
 * @return {Promise} if OK complaint
 *                   else {errCode, errMessage}
 */
 function getComplaints(page) {
     return new Promise(function (resolve, reject) {
         Complaint.count(function (err, total) {
             if (err) {
                 console.log(err);
                 return reject(5);
             }

             if (total === 0) {
                 return resolve({ total: total });
             }

             if (page >= total) {
                    page = total - 1;
             }

             var query = Complaint.find().sort({ weight: -1 }).lean();
             paging.setPaging(query, { limit: 1, page: page });

             query.exec(function (err, complaint) {
                 if (err) {
                     console.log(err);
                     return reject(5);
                 }

                 complaint = complaint[0];

                 getQuery[complaint.type](complaint.denounced.toString()).then(function (item) {
                      complaint.denounced = item;
                      item.isActive = undefined;
                      return resolve({ complaint: complaint, total: total });
                 },

                 function (err) {
                     console.log(err);
                     if (err === 1) {
                         Complaint.remove({ _id: complaint._id }, function (err) {
                             if (err) {
                                 console.log(err);
                                 return reject(5);
                             }

                             module.exports.getComplaints(page).then(function (complaint) {
                                 return resolve(complaint);
                             },

                             function (err) {
                                 return reject(err);
                             });
                         });
                     } else {
                         return reject(5);
                     }
                 });
             });
         });
     });
 }

 /**
 * Discards a complaint.
 * @param {ObjectId} id - Id of complaint to discard.
 * @return {Promise} if OK simply resolves
 *                   else {errCode, errMessage}
 */
function discardComplaint(id) {
    return new Promise(function (resolve, reject) {
        Complaint.findByIdAndRemove(id, function (err, complaint) {
            if (err) {
                console.log(err);
                return reject(5);
            }

            return resolve();
        });
    });
}

/**
* Accepts a complaint and removes related content, then removes complaint.
* @param {ObjectId} id - Id of complaint to accept.
* @return {Promise} if OK returns object with id's of removed items.
*                   else {errCode, errMessage}
*/
function acceptComplaint(id) {
    return new Promise(function (resolve, reject) {
        Complaint.findById(id, function (err, complaint) {
            if (err) {
                console.log(err);
                return reject(5);
            }

            if (!complaint) {
                return reject(1);
            }

            deleteQuery[complaint.type](complaint.denounced.toString()).then(function (removed) {
                 complaint.remove(function (err) {
                     if (err) {
                         console.log(err);
                         return reject(5);
                     }

                     return resolve(removed);
                 });
             },

             function (err) {
                 return reject(err);
             });
         });
    });
}

module.exports = {
    createComplaint: createComplaint,
    getComplaints: getComplaints,
    discardComplaint: discardComplaint,
    acceptComplaint: acceptComplaint
};
