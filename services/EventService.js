'use strict';
var Event = require('../models/event');
var Group = require('../models/group');
var fs = require('fs');
var qr = require('qr-image');
var qrCodesPath = require('../config.js').QRImageStorage;
var uuid = require('uuid');

/**
* Creates a new event.
* @param {Object} data - {
*     {String} title- name of the event,
*     {String} description - description of the event,
*     {String} url - link to page with more info about the event,
*     {Date} date - date when event is carried output,
*     {Array} tags - array of tags of the event,
*     {ObjectId} group - _id of the group associated to the event,
*     {String} image - path to the image of the event from rootdir,
*     {Float} lat - latitude of location where event is carried out,
*     {Float} lon - longitude of location where event is carried out,
*     {Boolean} qr - truthy if owner wants to generate qr, else falsy
*     {String} price - price to access the event,
*     {String} age - recomended age to attend the event
* }
* @return {Promise} if OK new event's ID
*                   else {errCode}
*/
function createEvent(data) {
    return new Promise(function (resolve, reject) {
        Event.create(data, (err, doc) => {
            if (err) {
                console.log(err);
                return reject(5);
            }

            if (data.qr === '1') {
                if (!fs.existsSync(qrCodesPath)) {
                    fs.mkdirSync(qrCodesPath);
                }

                var name = qrCodesPath + uuid.v1() + '.png';
                var code = qr.image(doc._id.toString(), { type: 'png' });
                var output = fs.createWriteStream(name);
                code.pipe(output);
                Event.update({ _id: doc._id },
                    { qrCode: name }, function (err) {
                        if (err) {
                            console.log(err);
                            Event.remove({ _id: doc._id });
                        }
                });

            }

            if (doc) {
                Group.update({ _id: data.group },
                     { $push: { events: doc._id } }, (err, group) => {
                    if (err) {
                        console.log(err);
                        return reject(5);
                    }
                });
            }

            return resolve(doc._id);
        });
    });
}

/**
* Retrieves all the events with specified id's.
* @param idList - id or [ids] of events to retrieve.
* @param options - mongoose options object, so far accepts select
* @return {Promise} if OK [{Event where isActive=true}] ordered by _id DESC
*                   else {errCode}
*/
function getEvents(idList, options) {
    return new Promise(function (resolve, reject) {
        var query;
        if (Array.isArray(idList)) {
            query = Event.find({ isActive: true,
                _id: { $in: idList }
            }).populate('group', 'name').sort({ _id: -1 });
        } else if (typeof idList === 'string') {
            query = Event.findOne({ isActive: true, _id: idList });
        }

        if (options && options.select) {
            query = query.select(options.select);
        }

        query.exec(function (err, objs) {
            if (err) {
                console.log(err);
                return reject(5);
            }

            if (!objs) {
                return reject(1);
            }

            return resolve(objs);
        });
    });
}

/**
* Retrieves all the events with specified id's.
* @param idList - id or [ids] of events to retrieve.
* @return {Promise} if OK [{Event where isActive=true}] ordered by _id DESC
*                   else {errCode}
*/
function getEventsFromGroups(idList) {
    return new Promise(function (resolve, reject) {
        var query;
        if (Array.isArray(idList)) {
            query = Event.find({ isActive: true,
                group: { $in: idList }
            });
        } else if (typeof idList === 'string') {
            query = Event.find({ isActive: true, group: idList });
        }

        query.populate('group', 'name').sort({ _id: -1 }).exec(function (err, objs) {
            if (err) {
                console.log(err);
                return reject(5);
            }

            if (!objs) {
                return reject(1);
            }

            return resolve(objs);
        });
    });
}

/**
* Modifies the event with specified id.
* @param {ObjectId} id - Id of the event to modify.
* @param {Object} data - {
*     {String} title- name of the event,
*     {String} description - description of the event,
*     {String} url - link to page with more info about the event,
*     {Date} date - date when event is carried output,
*     {Array} tags - array of tags of the event,
*     {ObjectId} group - _id of the group associated to the event,
*     {Float} lat - latitude of location where event is carried out,
*     {Float} lon - longitude of location where event is carried out,
*     {String} price - price to access the event,
*     {String} age - recomended age to attend the event
* }
* @param {Object} file - New image of the group.
* @return {Promise} if OK simply resolves
*                   else {errCode}
*/
function modifyEvent(id, data, file) {
    return new Promise(function (resolve, reject) {
        Event.findById(id).exec(function (err, event) {
            if (err) {
                console.log(err);
                return reject(5);
            }

            if (event) {
                if (file) {
                    var oldImage = event.image;
                    event.image = file.path;
                }

                event = Object.assign(event, data);
                event.save(function (err) {
                    if (err) {
                        console.log('error ', err);
                        return reject(5);
                    }

                    if (oldImage) {
                       fs.unlink(oldImage, function (err, ok) {
                         if (err) {
                           console.log('There is no file to delete.');
                         }
                       });
                   }

                    return resolve();
                });
            } else {
                return reject(5);
            }
        });
    });
}

/**
* Deactivates the events with specified ids.
* @param idList - Id or [id] of the events to deactivate.
* @return {Promise} if OK returns id or [ids] of removed events
*                   else {errCode}
*/
function removeEvents(idList) {
    return new Promise(function (resolve, reject) {
        var findQuery;
        var updateQuery;
        if (Array.isArray(idList)) {
            findQuery = Event.find({ _id: { $in: idList }, isActive: true,
                date: { $gte: new Date() } });
        } else if (typeof idList === 'string') {
            findQuery = Event.find({ _id: idList, isActive: true,
                date: { $gte: new Date() } }).limit(1);
        }

        findQuery.select('_id').exec((err, events) => {
            if (err) {
                console.log(err);
                return reject(5);
            }

            if (Array.isArray(idList)) {
                if (events.length === 0) {
                    return resolve([]);
                } else {
                    updateQuery =  Event.update({ _id: { $in: events } },
                        { isActive: false }, { multi: true });
                }
            } else if (typeof idList === 'string') {
                if (events.length === 0) {
                    return reject(1);
                } else {
                    updateQuery = Event.update({ _id: events[0] }, { isActive: false });
                }
            }

            updateQuery.exec((err) => {
                if (err) {
                    console.log(err);
                    this.restoreEvents(events).then(function () {
                        return reject(5);
                    });
                }

                return resolve(events);
            });
        });
    });
}

/**
* Restores the events with specified ids.
* @param idList - Id or [id] of the events to restore.
* @return {Promise} if OK returns id or [ids] of removed events
*                   else {errCode}
*/
function restoreEvents(idList) {
    return new Promise(function (resolve, reject) {
        var updateQuery;
            if (Array.isArray(idList)) {
                updateQuery =  Event.update({ _id: { $in: idList } },
                    { isActive: true }, { multi: true });
            } else if (typeof idList === 'string') {
                updateQuery = Event.update({ _id: idList }, { isActive: true });
            }

            updateQuery.exec(function (err) {
                if (err) {
                    console.log(err);
                    return reject(5);
                }

                return resolve(idList);
            });
    });
}

/**
* Get the url of the QR code from one event.
* @param {ObjectId} eventId - _id of the event which have the desired QR code.
* @param {ObjectId} userId - _id of the user who wants the QR code.
* @return {Promise} if OK returns the url of the QR code
*                   else {errCode}
*/
function getQR(eventId, userId) {
    return new Promise(function (resolve, reject) {
        Event.findById(eventId).populate('group', 'owner').exec((err, event) => {
            if (err) {
                console.log(err);
                return reject(5);
            }

            if (!event) {
                return reject(1);
            }

            if (!event.group.owner.equals(userId)) {
                return reject(2);
            }

            if (!event.qrCode) {
                return reject(3);
            }

            return resolve(event.qrCode);
        });
    });
}

module.exports = {
    createEvent: createEvent,
    getEvents: getEvents,
    getEventsFromGroups: getEventsFromGroups,
    modifyEvent: modifyEvent,
    removeEvents: removeEvents,
    restoreEvents: restoreEvents,
    getQR: getQR
};
