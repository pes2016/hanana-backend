'use strict';

const _ = require('lodash');
const Utils = require('../helpers/utils');
const paging = require('../handlers/paging-handler');
const User = require('../models/user');
const fs = require('fs');
const Triggers = require('../triggers/executer');

module.exports = {
    updateAvatar: function (user, file, req) {
        return new Promise(function (resolve, reject) {
            let oldAvatarPath = user.avatar;
            user.avatar = file.path;

            user.save((err, doc) => {
                if (err) {
                    reject(err);
                }

                if (oldAvatarPath) {
                    fs.unlink(oldAvatarPath, function (err, ok) {
                        if (err) {
                            console.log('Error deleting file ', err);
                        }
                    });
                }

                Triggers.execute(req, Triggers.triggerKinds.PROFILE, [])
                    .then((completedBadges) => {
                        console.log('Trigger executed okai');
                    }, (err) => {
                        console.log('Trigger executed wrong');
                    });

                return resolve(doc.avatar);
            });
        });
    },

    closeAccount: function (user) {
        return new Promise(function (resolve, reject) {
            user.isActive = false;
            user.save((err, doc) => {
                if (err) {
                    return reject(err);
                }

                return resolve();
            });
        });
    },

    updateAbout: function (user, about, req) {
        return new Promise(function (resolve, reject) {
            user.about = about.substring(0, 100);
            user.save((err, doc) => {
                if (err) {
                    return reject(err);
                }

                Triggers.execute(req, Triggers.triggerKinds.PROFILE, [])
                    .then((completedBadges) => {
                        console.log('Trigger executed okai');
                    }, (err) => {
                        console.log('Trigger executed wrong');
                    });
                return resolve();
            });
        });
    },

    togglePrivacity: function (user) {
        return new Promise(function (resolve, reject) {
            user.isPrivate = !user.isPrivate;

            //Switched to public
            if (!user.isPrivate) {
                user.followers = _.unionWith(user.followers, user.pending, Utils._idComparator);
                user.pending = [];
                user.discarded = [];
                user.markModified('followers');
                user.markModified('pending');
                user.markModified('discarded');
            }

            user.save((err, doc) => {
                if (err) {
                    return reject(err);
                }

                return resolve(doc.isPrivate);
            });
        });
    },

    toggleNotifications: function (user) {
        return new Promise(function (resolve, reject) {
            user.pushNotifications = !user.pushNotifications;
            user.save((err, doc) => {
                if (err) {
                    return reject(err);
                }

                return resolve(doc.pushNotifications);
            });
        });
    },

    updateInterests: function (user, interests, req) {
        return new Promise(function (resolve, reject) {
            user.interests = interests;
            user.markModified('interests');
            user.save((err, doc)=> {
                if (err) {
                    console.log('error ' + err);
                    return reject(err);
                }

                Triggers.execute(req, Triggers.triggerKinds.INTERESTS, [])
                    .then((completedBadges) => {
                        console.log('Trigger executed okai');
                    }, (err) => {
                        console.log('Trigger executed wrong');
                    });
                return resolve(doc);

            });
        });
    }
};
