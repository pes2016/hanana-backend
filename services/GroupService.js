'use strict';
var Group = require('../models/group');
var EventService = require('./EventService');
var uuid = require('uuid');
var fs = require('fs');

/**
* Creates a new group.
* @param {Object} data - {
*     {String} name - name of the group,
*     {String} description - description of the group,
*     {String} image - path to the image of the group from rootdir
* }
* @param {User} ownerUser - User that owns the group.
* @return {Promise} if OK new group's ID
*                   else {errCode}
*/
function createGroup(data, ownerUser) {
    return new Promise(function (resolve, reject) {
        var newGroup = new Group({
            id: uuid.v4(),
            name: data.name,
            description: data.description,
            image: data.image,
            owner: ownerUser._id
        });

        newGroup.save((err, group) => {
            if (err) {
                console.log(err);
                if (err.code === 11000) {
                    return reject(0);
                }

                return reject(5);
            }

            ownerUser.ownedGroups.push(newGroup._id);
            ownerUser.save((err, user) => {
                if (err) {
                    newGroup.remove({ _id: newGroup._id });
                    console.log(err);
                    return reject(5);
                }

                return resolve(newGroup._id);
            });
        });
    });
}

/**
* Retrieves all the groups with specified id's.
* @param idList - id or [ids] of groups to retrieve.
* @param options - mongoose options object, so far accepts select
* @return {Promise} if OK [{Groups where isActive=true}] ordered by _id DESC
*                   else {errCode}
*/
function getGroups(idList, options) {
    return new Promise(function (resolve, reject) {
        var query;
        if (Array.isArray(idList)) {
            query = Group.find({ isActive: true,
                _id: { $in: idList }
            }).sort({ _id: -1 });
        } else if (typeof idList === 'string') {
            query = Group.findOne({ isActive: true, _id: idList });
        }

        if (options && options.select) {
            query = query.select(options.select);
        }

        query.exec(function (err, objs) {
            if (err) {
                console.log(err);
                return reject(5);
            }

            if (!objs) {
                return reject(1);
            }

            return resolve(objs);
        });
    });
}

/**
* Modifies the group with specified id.
* @param {ObjectId} id - Id of the group to modify.
* @param {Object} data - {
*     {String} name - name of the group,
*     {String} description - description of the group,
*     {String} image - path to the image of the group from rootdir
* }
* @param {Object} file - New image of the group.
* @return {Promise} if OK simply resolves
*                   else {errCode}
*/
function modifyGroup(id, data, file) {
    return new Promise(function (resolve, reject) {
        Group.findById(id).exec((err, obj) => {
            if (err) {
                console.log(err);
                return reject(5);
            }

            if (!obj) {
                return reject(1);
           }

            if (file) {
                var oldGroupImagePath = obj.image;
                obj.image = file.path;
            }

            obj = Object.assign(obj, data);
            obj.save(function (err) {
                if (err) {
                    console.log(err);
                    if (err.code === 11000) {
                        return reject(0);
                    }

                    return reject(5);
                }

                 if (oldGroupImagePath) {
                    fs.unlink(oldGroupImagePath, function (err, ok) {
                      if (err) {
                        console.log('There is no file to delete.');
                      }
                    });
                }

                return resolve();
            });
        });
    });
}

/**
* Deactivates the groups with specified ids and all it's future events.
* @param idList - Id or [id] of the groups to deactivate.
* @return {Promise} if OK returns {[removedEvents], [removedGroups]}
*                   else {errCode}
*/
function removeGroups(idList) {
    return new Promise(function (resolve, reject) {
        var delGroups;
        var findQuery;
        var updateQuery;
        if (Array.isArray(idList)) {
            findQuery = Group.find({ _id: { $in: idList }, isActive: true });
        } else if (typeof idList === 'string') {
            findQuery = Group.find({ _id: idList, isActive: true }).limit(1);
        }

        findQuery.exec((err, groups) => {
            if (err) {
                console.log(err);
                return reject(5);
            }

            delGroups = groups.map(function (group) {
                return group._id;
            });

            var events = [];
            groups.forEach(function (group) {
                events = events.concat(group.events);
            });

            EventService.removeEvents(events).then(function (events) {

                if (Array.isArray(idList)) {
                    if (delGroups.length === 0) {
                        return resolve({ events: [], groups: [] });
                    } else {
                        updateQuery = Group.update({ _id: { $in: delGroups } },
                             { isActive: false }, { multi: true });
                    }
                } else if (typeof idList === 'string') {
                    if (delGroups.length === 0) {
                        return reject(5);
                    } else {
                        updateQuery = Group.update({ _id: { $in: delGroups } },
                             { isActive: false });
                    }
                }

                updateQuery.exec((err) => {
                    if (err) {
                        console.log(err);
                        var promises = [];
                        promises.push(EventService.restoreEvents(events));
                        promises.push(this.restoreGroups(delGroups));
                        Promise.all(promises).then(function () {
                            return reject(5);
                        });
                    }

                    return resolve({ events: events, groups: delGroups });
                });
            },

            function (err) {
                return reject(err);
            });
        });
    });
}

/**
* Restores the groups with specified ids.
* @param idList - Id or [id] of the events to restore.
* @return {Promise} if OK returns id or [ids] of removed events
*                   else {errCode}
*/
function restoreGroups(idList) {
    return new Promise(function (resolve, reject) {
        var updateQuery;
            if (Array.isArray(idList)) {
                updateQuery =  Group.update({ _id: { $in: idList } },
                    { isActive: true }, { multi: true });
            } else if (typeof idList === 'string') {
                updateQuery = Group.update({ _id: idList }, { isActive: true });
            }

            updateQuery.exec(function (err) {
                if (err) {
                    console.log(err);
                    return reject(5);
                }

                return resolve(idList);
            });
    });
}

module.exports = {
    getGroups: getGroups,
    createGroup: createGroup,
    modifyGroup: modifyGroup,
    removeGroups: removeGroups,
    restoreGroups: restoreGroups
};
