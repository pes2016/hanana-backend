'use strict';
var User = require('../models/user');
var GroupService = require('./GroupService');
var encrypt = require('../helpers/encryption.js');
var mailer = require('../handlers/email-handlers.js');
var uuid = require('uuid');
const paging = require('../handlers/paging-handler');
const UserQueryHelper = require('../helpers/user-query-helper');

/**
* Retrieves one or several premium users.
* @param {ObjectId} id - id of the premium user that you want to get.
*                         Or undefined if you want to get all the premium users.
* @return {Promise} if OK [{Users where isActive=true and isPremium=true}]
*                    ordered by _id DESC
*                   else {errCode}
*/
function getPremiumUsers(id) {
    return new Promise(function (resolve, reject) {

        var query;
        if (id) {
            query = User.findById({
                isPremium: true,
                isActive: true,
                _id: id
            });
        } else {
            query = User.find({
                isPremium: true,
                isActive: true
            }).sort({
                _id: -1
            });
        }

        query.exec((err, objs) => {
            if (err) {
                return reject(5);
            }

            if (!objs) {
                return reject(1);
            }

            return resolve(objs);
        });
    });
}

/**
* Retrieves one or several regular users.
* @param {ObjectId} idList - id of the regular user/s that you want to get.
*                         Or undefined if you want to get all the regular users.
* @param options - mongoose options object, so far accepts select
* @return {Promise} if OK [{Users where isActive=true and isPremium=false and isAdmin=false}]
*                    ordered by _id DESC
*                   else {errCode}
*/
function getUsers(idList, options) {
    return new Promise(function (resolve, reject) {

        var query;
        if (!idList) {
            query = User.find({
                isAdmin: false,
                isPremium: false,
                isActive: true
            }).sort({
                _id: -1
            });
        } else if (Array.isArray(idList)) {
            query = User.find({
                isAdmin: false,
                isPremium: false,
                isActive: true
            });
        } else if (typeof idList === 'string') {
            query = User.findOne({
                isAdmin: false,
                isPremium: false,
                isActive: true,
                _id: idList
            });
        }

        if (options && options.select) {
            query = query.select(options.select);
        }

        query.exec((err, objs) => {
            if (err) {
                return reject(5);
            }

            if (!objs) {
                return reject(1);
            }

            return resolve(objs);
        });
    });
}

/**
* Modifies the user with specified id.
* @param {ObjectId} id - id of the user that you want to modify.
* @param {Object} data - {
*     {String} email - email of the user.
* }
* @return {Promise} if OK simply resolves
*                   else {errCode}
*/
function modifyUser(id, data) {
    return new Promise(function (resolve, reject) {
        User.findById(id).exec((err, obj) => {
            if (err) {
                console.log(err);
                return reject(5);
            }

            if (!obj) {
                return reject(1);
            }

            obj = Object.assign(obj, data);
            obj.save(function (err) {
                if (err) {
                    console.log(err);
                    if (err.code === 11000) {
                        return reject(0);
                    }

                    return reject(5);
                }

                return resolve();
            });
        });
    });
}

/**
* Generates a new random password for the specified user and sends him an email with it.
* @param {ObjectId} id - id of the premium user whose password is to be reset.
* @return {Promise} if OK simply resolves
*                   else {errCode}
*/
function resetPassword(id) {
    return new Promise(function (resolve, reject) {
        User.findById(id).exec((err, obj) => {
            if (err) {
                console.log(err);
                return reject(5);
            }

            if (obj) {

                encrypt.generatePassword().then(function (password) {
                    obj.password = password.encrypted;
                    obj.save(function (err) {
                        if (err) {
                            console.log(err);
                            return reject(5);
                        }

                        mailer.sendNewPassword(obj.email, obj.name, password.unencrypted)
                          .then(function () {
                                return resolve();
                            },

                            function (err) {
                                return reject(err);
                            });
                    });
                },

                function (err) {
                    return reject(err);
                }
            );

            } else {
                return reject(1);
            }
        });
    });
}

/**
* Creates a new premium user with the name and the email provided.
* @param {String} name - name of the premium user that you want to create.
* @param {String} email - email of the premium user that you want to create.
* @return {Promise} if OK returns _id from the new user.
*                   else {errCode}
*/
function createPremiumUser(name, email) {
    return new Promise(function (resolve, reject) {
        var newUser = new User({
            id: uuid.v4(),
            name: name,
            email: email,
            isPremium: true,
        });

        encrypt.generatePassword().then(function (password) {
            newUser.password = password.encrypted;

            newUser.save((err, doc) => {
                if (err) {
                    if (err.code === 11000) {
                        return reject(0);
                    }

                    return reject(5);
                }

                mailer.sendNewPassword(doc.email, doc.name, password.unencrypted)
                .then(function () {
                    return resolve(newUser._id);
                },

                function (err) {
                    newUser.remove({ _id: newUser._id });
                    return reject(err);
                });
            });
        });
    });
}

/**
* Deactivates the user with specified id.
* @param idList - Id of the user to deactivate.
* @return {Promise} if OK returns {removedUser, [removedEvents], [removedGroups]}
*                   else {errCode}
*/
function removeUser(id) {
    return new Promise(function (resolve, reject) {
        User.findById(id).exec((err, user) => {
            if (err) {
                console.log(err);
                return reject(5);
            }

            if (!user) {
                return reject(1);
            }

            GroupService.removeGroups(user.ownedGroups).then(function (removed) {
                User.update({ _id: user._id }, { isActive: false }).exec(function (err) {
                    if (err) {
                        console.log(err);
                        return reject(5);
                    }

                    return resolve({ user: user._id, events: removed.events,
                         groups: removed.groups });
                     });
            },

            function (err) {
                return reject(err);
            });
        });
    });
}

/**
* Returns the ranking of users
* @return {Promise} if OK returns [users]
*                   else {errCode}
*/
function getRanking(params) {
    return new Promise(function (resolve, reject) {

        var RankingQuery = User.find({}).sort({ points: -1 });
        UserQueryHelper.selectActiveAppUsers(RankingQuery);
        paging.setPaging(RankingQuery, params);
        RankingQuery.exec((err, users) => {
            if (err) {
                console.log(err);
                return reject(5);
            }

            resolve({ users: users });
        });
    });
}

module.exports = {
    getPremiumUsers: getPremiumUsers,
    getUsers: getUsers,
    modifyUser: modifyUser,
    resetPassword: resetPassword,
    createPremiumUser: createPremiumUser,
    removeUser: removeUser,
    getRanking: getRanking
};
