/**
 * Created by marcos on 03/12/2016.
 */
'use strict';
const Event = require('../models/event');
const EventQueryHelper = require('../helpers/event-query-helper');
const paging = require('../handlers/paging-handler');
const UserService = require('./UserService');
var _ = require('lodash');

function handleEventsListPromise(resolve) {
    return (err, docs) => {
        if (err) {
            console.log('Error executing promise ' + err);
        }

        console.log('-----------------------------');
        _.forEach(docs, (o)=> {
          console.log(o._id + ' ' + o.title + ' at ' + o.lat + ' ' + o.lon);
        });
        console.log('-----------------------------');
        console.log('Returning ', (docs || []).length, ' documents');
        resolve(docs || []);
    };
}

function searchExecutor(f) {
  return function (params, user) {
    return new Promise(function (resolve, reject) {
      f(params, user)
        .then(function (values) {
          resolve({ events: values });
        });
    });
  };
}

function _getSuggestedEvents(params, user) {
    return new Promise(function (resolve, reject) {
        var EventQuery = Event.find();
        EventQueryHelper.searchBy(EventQuery, params, false);

        UserService.getUsers(user.following).then((users) => {
            var interests = [];
            _.each(users, (us) => {
                interests = _.concat(interests, us.interests);
            });
            interests = _.uniqBy(_.concat(interests, user.interests), function (int) {
                return int.toString();
            });

            EventQueryHelper.filterByInterests(EventQuery, interests);
            EventQueryHelper.selectFutureEvents(EventQuery);
            paging.setPaging(EventQuery, params);
            EventQuery.exec(handleEventsListPromise(resolve));
        });
    });
}

function _getPopularEvents(params, user) {
    return new Promise(function (resolve, reject) {
        var EventQuery = Event.find();
        EventQueryHelper.searchBy(EventQuery, params);
        EventQueryHelper.selectFutureEvents(EventQuery);
        paging.setPaging(EventQuery, params);
        EventQuery.exec(handleEventsListPromise(resolve));
    });
}

function _getFriendsEvents(params, user) {
    return new Promise(function (resolve, reject) {
        var EventQuery = Event.find();
        EventQueryHelper.searchBy(EventQuery, params);
        EventQueryHelper.filterByFollowing(EventQuery, user.following);
        EventQueryHelper.selectFutureEvents(EventQuery);
        paging.setPaging(EventQuery, params);
        EventQuery.exec(handleEventsListPromise(resolve));
    });
}

function _getGeolocatedEvents(params, user) {
    return new Promise(function (resolve, reject) {
      console.log('params are ', params);
        var EventQuery = Event.find().limit(100);
        EventQueryHelper.searchBy(EventQuery, params);
        EventQueryHelper.selectFutureEvents(EventQuery);
        EventQueryHelper.selectNearSphereEvents(EventQuery, params);
        EventQuery.exec(handleEventsListPromise(resolve));
    });
}

function getPastEventsByGroup(groupId, params) {
    return new Promise(function (resolve, reject) {
        var EventQuery = Event.find();
        EventQueryHelper.selectByGroup(EventQuery, groupId);
        EventQueryHelper.selectPastEvents(EventQuery);
        EventQueryHelper.sortByDate(EventQuery);
        paging.setPaging(EventQuery, params);
        EventQuery.exec(handleEventsListPromise(resolve));
    });
}

function getFutureEventsByGroup(groupId, params) {
    return new Promise(function (resolve, reject) {
        var EventQuery = Event.find();
        EventQueryHelper.selectByGroup(EventQuery, groupId);
        EventQueryHelper.selectFutureEvents(EventQuery);
        EventQueryHelper.sortByDate(EventQuery);
        paging.setPaging(EventQuery, params);
        EventQuery.exec(handleEventsListPromise(resolve));
    });
}

function getFutureAndPastEventsByGroup(groupId, params) {
    return new Promise(function (resolve, reject) {
            let promises = [
                getPastEventsByGroup(groupId, params),
                getFutureEventsByGroup(groupId, params)
            ];

            Promise.all(promises)
                .then(
                    function (values) {
                        return resolve({
                            pastEvents: values[0],
                            futureEvents: values[1]
                        });
                    });
        }
    );
}

var getFriendsEvents = searchExecutor(_getFriendsEvents);
var getPopularEvents = searchExecutor(_getPopularEvents);
var getSuggestedEvents = searchExecutor(_getSuggestedEvents);
var getGeolocatedEvents = searchExecutor(_getGeolocatedEvents);

module.exports = {
    getFutureAndPastEventsByGroup: getFutureAndPastEventsByGroup,
    getGeolocatedEvents: getGeolocatedEvents,
    getFriendsEvents: getFriendsEvents,
    getPopularEvents: getPopularEvents,
    getSuggestedEvents: getSuggestedEvents
};
