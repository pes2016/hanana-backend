/**
 * Created by marcos on 03/12/2016.
 */

'use strict'
const fs = require('fs');
const Event = require('../models/event');
const Triggers = require('../triggers/executer');

module.exports = {
    createEvent: function (data, userId, req) {
        return new Promise(function (resolve, reject) {
            let event = Object.assign({},
                data, {
                    user: userId
                },
                { assistants: [userId] },
                {
                    isPrivate: true
                });
            let entity = new Event(event);
            entity.save((err, doc) => {
                if (err) {
                    reject(err);
                }

                Triggers.execute(req, Triggers.triggerKinds.CREATE_PRIVATE, [])
                    .then((completedBadges) => {
                        console.log('Trigger executed okai');
                    }, (err) => {
                        console.log('Trigger executed wrong');
                    });

                resolve(doc);
            });
        });
    },

    updateEventImage: function (eventId, file) {
        return new Promise(function (resolve, reject) {
            Event.findById(eventId, (err, doc) => {
                if (err) {
                    console.log('error ', err);
                    reject(err);
                }

                var oldAvatarPath = doc.image;

                doc.image = file.path;

                doc.save((err, doc) => {
                    if (err) {
                        reject(err);
                    }

                    if (oldAvatarPath) {
                        fs.unlink(oldAvatarPath, function (err, ok) {
                            if (err) {
                                console.log('There is no file to delete');
                            }
                        });
                    }

                    resolve(doc);
                });
            });
        });
    }
};
