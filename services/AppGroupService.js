/**
 * Created by marcos on 03/12/2016.
 */
'use strict'

const GroupQueryHelper = require('../helpers/group-query-helper');
const paging = require('../handlers/paging-handler');
const Group = require('../models/group');
const AppEventService = require('../services/AppEventService');
const as = require('async');
const _ = require('lodash');
const Utils = require('../helpers/utils');
const EventsFinderService = require('../services/EventsFinderService');
const UserUtils = require('../helpers/user-utils');
const Triggers = require('../triggers/executer');

module.exports = {
    discoverGroups: function (params, user) {
        return new Promise(function (resolve, reject) {
            var query = Group.find();
            paging.setPaging(query, params);
            GroupQueryHelper.searchBy(query, params);
            query.lean().exec(function (err, docs) {
                if (err) {
                    return reject(err);
                }

                //Compute the number of events for each group
                as.map(docs,
                    (o, cb) => {

                        //Number of followers
                        o.followers = o.followers || [];
                        o.numberOfFollowers = o.followers.length;

                        //Number of events
                        AppEventService.getNumberOfEventsByGroup(o._id)
                            .then(function (data) {
                                o.numberOfEvents = data;
                                return cb(null, o);
                            }, function (err) {

                                return cb(err);
                            });
                    },

                    (err, docs) => {
                        if (err) {
                            return reject(err);
                        } else {
                            return resolve({
                                groups: docs
                            });
                        }
                    });
            });
        });
    },

    toggleFollow: function (groupId, user, req) {
        console.log('group id ', groupId);
        return new Promise(function (resolve, reject) {
            Group.findById(groupId).exec((err, group) => {
                Utils.toggleArray(Group, 'followers', groupId, user._id, (isFollowing) => {
                    if (isFollowing) {
                        user.followingGroups = user.followingGroups || [];
                        user.followingGroups[user.followingGroups.length] = group._id;
                    } else {
                        _.remove(user.followingGroups, function (o) {
                            return o.equals(groupId);
                        });
                    }

                    user.markModified('followingGroups');
                    user.save((err, doc) => {
                        if (err) {
                            return reject(err);
                        }

                        //TODO NO VA
                        Triggers.execute(req, Triggers.triggerKinds.FOLLOW_GROUP,
                            [group.owner, group._id, null])
                            .then((completedBadges) => {
                                console.log('Trigger executed okai');
                            }, (err) => {
                                console.log('Trigger executed wrong');
                            });

                        return resolve(isFollowing);
                    });
                });

            });
        });
    },

    getGroup: function (groupId, user, params) {
        return new Promise(function (resolve, reject) {
            Group.findById(groupId).lean().exec((err, doc) => {
                if (err) {
                    console.log('error ' + err);
                    return reject(err);
                }

                doc.numberOfFollowers = (doc.followers || []).length;

                doc.isFollowing = Utils.isIdInArray(user._id, doc.followers);

                EventsFinderService.getFutureAndPastEventsByGroup(groupId, params)
                    .then(
                        function (events) {
                            doc.futureEvents = events.futureEvents;
                            doc.pastEvents = events.pastEvents;
                            AppEventService.getNumberOfEventsByGroup(groupId)
                                .then(function (count) {
                                    doc.numberOfEvents = count;
                                    return resolve(doc);
                                }, function (err) {

                                    return reject(err);
                                });
                        }
                    );
            });
        });
    },

    getFollowers: function (groupId, user, params) {
        return new Promise(function (resolve, reject) {
            var GroupQuery = Group.findById(groupId);
            paging.setPaging(GroupQuery, params);
            GroupQuery.populate('followers');
            GroupQuery.lean();
            GroupQuery.exec((err, doc) => {
                if (err) {
                    console.log('error ' + err);
                    return reject(err);
                }

                var list = doc.followers;

                UserUtils.removeUsersThatBlockedMe(list, user._id);
                UserUtils.calculateIfAlreadyFollowing(list, user._id);

                resolve({ users: list });
            });
        });
    },

    shareGroup: function shareGroup(groupdId, req) {
        return new Promise(function (resolve, reject) {
            console.log('Share group ', groupdId);
            Group.findById(groupdId).exec((err, doc) => {
                if (err) {
                    return reject(err);
                }

                console.log('doc is ', doc);
                if (doc) {
                    Triggers.execute(req, Triggers.triggerKinds.SHARE, [doc.owner])
                        .then((completedBadges) => {
                            console.log('Trigger executed okai');
                            return resolve(true);
                        }, (err) => {
                            console.log('Trigger executed wrong');
                            return reject(err);
                        });
                }
            });
        });
    }

};
