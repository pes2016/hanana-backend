/**
 * Created by marcos on 03/12/2016.
 */
'use strict';
const Event = require('../models/event');
const EventQueryHelper = require('../helpers/event-query-helper');
const paging = require('../handlers/paging-handler');
const _ = require('lodash');

const Utils = require('../helpers/utils');
const UserUtils = require('../helpers/user-utils');
const Triggers = require('../triggers/executer');
const mongoose = require('mongoose');

const EVENT_POPULATE_INFO = {
    path: 'rates.user',
    select: 'name avatar'
};

const RATE_USER_POPULATE_INFO = { path: 'user', select: 'name avatar' };

function computeHiddenThreshold(o) {
    return o.numberOfUpvotes * 2 < o.numberOfDownvotes;
}

module.exports = {
    getNumberOfEventsByGroup: function (groupId) {
        return new Promise(function (resolve, reject) {
            Event.count({
                group: groupId
            }, (err, count) => {
                if (err) {
                    return reject(err);
                }

                return resolve(count);
            });
        });
    },

    getEventsForUser: function (user, type, params) {
        return new Promise(function (resolve, reject) {
            let EventQuery = Event.find();
            switch (type) {
                case 'future':
                    EventQueryHelper.selectFutureEvents(EventQuery);
                    EventQueryHelper.sortByDate(EventQuery, 1);
                    break;
                case 'past':
                    EventQueryHelper.selectPastEvents(EventQuery);
                    EventQueryHelper.sortByDate(EventQuery, -1);
                    break;
                default:
                    EventQueryHelper.sortByDate(EventQuery);
            }
            EventQueryHelper.selectEventsForUser(EventQuery, user._id);
            paging.setPaging(EventQuery, params);
            EventQuery.exec((err, docs) => {
                if (err) {
                    return reject(err);
                }

                return resolve(docs);
            });
        });
    },

    getEvent: function (eventId, user) {
        return new Promise(function (resolve, reject) {
            Event.findById(eventId)
                .populate(EVENT_POPULATE_INFO)
                .lean()
                .exec((err, doc) => {
                    console.log('Doc is ', doc);
                    if (err) {
                        return reject(err);
                    }

                    let assistants = doc.assistants || [];
                    doc.willAssist = Utils.isIdInArray(user._id, doc.assistants);

                    //Number of friends assisting to the event.
                    let following = user.following || [];
                    doc.numberOfFriends = _.intersection(assistants, following).length;

                    //HAVE I BEEN THERE?
                    doc.haveAttended = Utils.isIdInArray(user._id, doc.attendees);

                    //HAVE I VOTED?
                    doc.myRate = _.find(doc.rates, function (rate) {
                        return rate.user._id.equals(user._id);
                    });

                    let rates = doc.rates || [];
                    doc.numberOfRates = rates.length;

                    if (doc.myRate) {
                        doc.myRate = Object.assign({}, doc.myRate);
                        _.remove(doc.rates, function (o) {
                            return o === doc.myRate._id;
                        });
                    }

                    return resolve(doc);
                });
        });

    },

    rateEvent: function (eventId, userId, rating) {
        return new Promise(function (resolve, reject) {
            let newRate = Object.assign({}, rating, {
                user: userId
            });

            Event.findById(eventId).exec((err, doc) => {
                _.remove(doc.rates, function (rate) {
                    return rate.user.equals(newRate.user);
                });

                doc.rates[doc.rates.length] = newRate;
                doc.markModified('rates');
                doc.save((err) => {
                    if (err) {
                        console.log('Error inserting rate', err);
                        return reject(err);
                    }

                    return resolve();
                });
            });
        });
    },

    toggleAssist: function (eventId, userId) {
        return new Promise(function (resolve, reject) {
            Utils.toggleArray(Event, 'assistants', eventId, userId, function (isAssisting) {
                return resolve(isAssisting);
            });
        });
    },

    confirmQR: function (eventId, userId, req) {
        return new Promise(function (resolve, reject) {
            Event.markAsAttendee(eventId, userId, (err, isAttending) => {
                if (err) {
                    console.log('error ', err);
                    return reject(err);
                }

                //TODO #TRIGGER TO TEST
                Event.findById(eventId).populate('group').exec((err, doc) => {
                    if (doc) {
                        Triggers.execute(req, Triggers.triggerKinds.ATTEND_EVENT,
                            [doc.group.owner, doc.group._id, eventId])
                            .then((completedBadges) => {
                                console.log('Trigger executed okai');
                            }, (err) => {
                                console.log('Trigger executed wrong');
                            });
                    }
                });

                return resolve(isAttending);
            });
        });
    },

    getAssistants: function (eventId, userId, params) {
        return new Promise(function (resolve, reject) {
            var EventQuery = Event.findById(eventId);
            paging.setPaging(EventQuery, params);
            EventQuery.populate('assistants');
            EventQuery.exec((err, doc) => {
                if (err) {
                    console.log('error ', err);
                    return reject(err);
                }

                var list = doc.assistants;

                UserUtils.removeUsersThatBlockedMe(list, userId);
                UserUtils.calculateIfAlreadyFollowing(list, userId);

                return resolve({
                    users: list
                });
            });
        });
    },

    shareEvent: function (eventId, req) {
        return new Promise(function (resolve, reject) {

            Event.findById(eventId).populate('group').exec((err, doc) => {
                if (doc) {
                    Triggers.execute(req, Triggers.triggerKinds.SHARE,
                        [doc.group.owner, doc.group._id])
                        .then((completedBadges) => {
                            console.log('Trigger executed okai');
                            return resolve(true);
                        }, (err) => {
                            console.log('Trigger executed wrong');
                            return reject(err);
                        });
                }
            });
        });
    },

    voteRate: function (eventId, rateId, type, user) {
        return new Promise(function (resolve, reject) {
            Event.findById(eventId).exec((err, event) => {
                if (err) {
                    return reject(err);
                }

                let eventRates = event.rates;
                let rateToVote = eventRates.id(rateId);

                let mainArray = type === 'upvote' ? rateToVote.upvotes : rateToVote.downvotes;
                let secondaryArray = type === 'upvote' ? rateToVote.downvotes : rateToVote.upvotes;
                let check = false;

                //If it exists, we remove from the other array just in case.
                _.remove(secondaryArray, function (o) {
                    return o.equals(user._id);
                });

                if (Utils.isIdInArray(user._id, mainArray)) {
                    _.remove(mainArray, function (o) {
                        return o.equals(user._id);
                    });
                } else {
                    mainArray[mainArray.length] = user._id;
                    check = true;
                }

                event.markModified('rates');
                event.save((err, data) => {
                    if (err) {
                        return reject(err);
                    }

                    return resolve(check);
                });
            });
        });
    },

    getRatesForEvent: function getRatesForEvent(eventId, user, params) {
        return new Promise(function (resolve, reject) {
            let page = params ? params.page || 0 : 0;
            let limit = params ? params.limit || 50 : 50;
            Event.aggregate([
                {
                    $match: {
                        _id: new mongoose.Types.ObjectId(eventId)
                    }
                },
                { $unwind: '$rates' },
                {
                    $project: {
                        _id: 1,
                        'rates._id': 1,
                        'rates.user': 1,
                        'rates.createdAt': 1,
                        'rates.comment': 1,
                        'rates.value': 1,
                        'rates.numberOfUpvotes': { $size: '$rates.upvotes' },
                        'rates.numberOfDownvotes': { $size: '$rates.downvotes' },
                        'rates.upvotes': 1,
                        'rates.downvotes': 1
                    }
                },
                { $sort: { 'rates.createdAt': -1 } },
                { $skip: page * limit },
                { $limit: limit },
                { $group: { _id: '$_id', rates: { $push: '$rates' } } }
            ], function (err, values) {
                if (err) {
                    console.log('Error aggregating ', err);
                    return reject(err);
                }

                let rates = values.length ? values[0].rates : [];

                console.log('Aggregation returned ', rates);
                _.map(rates, (o) => {
                    console.log('Iterating over ', o);

                    //Check if rate has to be downvoted
                    o.hidden = computeHiddenThreshold(o);
                    console.log('O.hidden is ', o.hidden);

                    //Check if i have voted this rate.
                    var vote = 0;
                    if (Utils.isIdInArray(user._id, o.upvotes)) {
                        vote = 1;
                    } else if (Utils.isIdInArray(user._id, o.downvotes)) {
                        vote = -1;
                    }

                    console.log('vote is ', vote);
                    o.voted = vote;

                    return o;
                });

                Event.populate(rates, RATE_USER_POPULATE_INFO, function (err, values) {
                    return resolve({ rates: values });
                });
            });
        });
    }
};
