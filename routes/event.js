'use strict'

var authHandler = require('../handlers/auth-handlers');
var controller = require('./../controllers/event');
var ImageStorage = require('../handlers/ImageStorage');
var multer = require('multer');

var directoryPath = require('../config.js').eventImageStorage;
var storage = new ImageStorage({
    directoryPath: directoryPath,
    destination: function (req, file, cb) {
        cb(null, directoryPath);
    },

    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
});
var upload = multer({
    storage: storage
});

module.exports = function (router) {

    router.get('/api/app/events/:id',
        authHandler.checkIfLoggedIn,
        controller.getEvent
    );

    router.post('/api/app/events/search/map',
        authHandler.checkIfLoggedIn,
        controller.getGeolocatedEvents);

    router.post('/api/app/events/search/friends',
        authHandler.checkIfLoggedIn,
        controller.getFriendsEvents);

    router.post('/api/app/events/search/popular',

    // authHandler.checkIfLoggedIn,
        controller.getPopularEvents);

    router.post('/api/app/events/search/suggested',
        authHandler.checkIfLoggedIn,
        controller.getSuggestedEvents);

    router.post('/api/app/events/:id/assistants',
        authHandler.checkIfLoggedIn,
        controller.getAssistants);

    router.post('/api/app/events/:id/qr',
        authHandler.checkIfLoggedIn,
        controller.confirmQR);

    router.post('/api/app/events/:id/assist',
        authHandler.checkIfLoggedIn,
        controller.toggleAssist);

    router.post('/api/app/events/:id/rate/:rateId/:type',
        authHandler.checkIfLoggedIn,
        controller.voteRate);

    router.post('/api/app/events/:id/rate/list',
        authHandler.checkIfLoggedIn,
        controller.getRatesForEvent);

    router.post('/api/app/events/:id/rate',
        authHandler.checkIfLoggedIn,
        controller.rateEvent);

    router.post('/api/app/events/:id/image',
        authHandler.checkIfLoggedIn,
        upload.single('file'),
        controller.updateEventImage);

      router.post('/api/app/events/:id/share',
          authHandler.checkIfLoggedIn,
          controller.shareEvent);

    router.post('/api/app/events/',
        authHandler.checkIfLoggedIn,
        controller.createEvent);

};
