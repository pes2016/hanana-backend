'use strict';

var authHandler = require('../handlers/auth-handlers');
var controller = require('./../controllers/premium');

var ImageStorage = require('../handlers/ImageStorage');
var multer = require('multer');
var directoryPath = require('../config.js').groupImageStorage;
var storage = new ImageStorage({
    directoryPath: directoryPath,
    destination: function (req, file, cb) {
        cb(null, directoryPath);
    },

    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
});
var upload = multer({ storage: storage });

var eventImagesDir = require('../config.js').eventImageStorage;
var eventImgStore = new ImageStorage({
    directoryPath: eventImagesDir,
    destination: function (req, file, cb) {
        cb(null, eventImagesDir);
    },

    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
});
var uploadEventImg = multer({ storage: eventImgStore });

module.exports = function (router) {

    router.post('/api/premium/notify/',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfPremium,
        controller.notifyUsers);

    router.post('/api/premium/createGroup',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfPremium,
        upload.single('file'),
        controller.createGroup);

    router.get('/api/premium/getOwnedGroups',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfPremium,
        controller.getOwnedGroups);

    router.get('/api/premium/getOwnedGroup/:id',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfPremium,
        controller.getOwnedGroup);

    router.post('/api/premium/modifyGroup/:id',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfPremium,
        upload.single('file'),
        controller.modifyGroup);

    router.delete('/api/premium/removeGroup/:id',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfPremium,
        controller.removeGroup);

    router.post('/api/premium/createEvent',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfPremium,
        uploadEventImg.single('image'),
        controller.createEvent);

    router.post('/api/events/:id',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfPremium,
        uploadEventImg.single('image'),
        controller.updateEvent);

    router.get('/api/premium/getQR/:id',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfPremium,
        controller.getQR);

    router.get('/api/premium/getOwnedEvents',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfPremium,
        controller.getOwnedEvents);

    router.get('/api/premium/getOwnedEvent/:id',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfPremium,
        controller.getOwnedEvent);

    router.delete('/api/premium/removeEvent/:id',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfPremium,
        controller.removeEvent);

    router.get('/api/premium/triggers',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfPremium,
        controller.getTriggers);

    router.get('/api/premium/badges',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfPremium,
        controller.getBadges);

    router.post('/api/premium/badges',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfPremium,
        upload.single('image'),
        controller.createBadge);

    router.get('/api/premium/triggers',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfPremium,
        controller.getTriggers);

};
