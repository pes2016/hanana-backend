'use strict'
var ensureAuth = require('../passport/ensure-authenticated');
var authHandler = require('../handlers/auth-handlers');
var emailHandler = require('../handlers/email-handlers');
var controller = require('./../controllers/user');
var jwt = require('jsonwebtoken');
var passport = require('passport');
var ImageStorage = require('../handlers/ImageStorage');
var multer = require('multer');
var fs = require('fs');

var utils = require('../helpers/utils');

var directoryPath = 'avatars/';
var storage = new ImageStorage({
    directoryPath: directoryPath,
    destination: function (req, file, cb) {
        cb(null, directoryPath);
    },

    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
});
var upload = multer({
    storage: storage
});

module.exports = function (router) {
    router.post('/api/ranking',
        authHandler.checkIfLoggedIn,
        controller.getUserRanking);

    router.post('/api/register/:provider', authHandler.register);
    router.get('/api/register/:provider', authHandler.register);
    router.post('/api/refreshToken', authHandler.refreshToken);
    router.post('/api/login', authHandler.login);

    router.get('/api/users/closeAccount',
        authHandler.checkIfLoggedIn,
        controller.closeAccount);

    router.get('/api/users/togglePrivacity',
        authHandler.checkIfLoggedIn,
        controller.togglePrivacity);

    router.get('/api/users/me',
        authHandler.checkIfLoggedIn,
        controller.me);

    router.get('/api/users/:id/interests',
        authHandler.checkIfLoggedIn,
        controller.getUserInterests);

    router.get('/api/users/:id',
        authHandler.checkIfLoggedIn,
        authHandler.isAllowedByUser,
        controller.getUserProfile);

    router.post('/api/users/toggleNotifications',
        authHandler.checkIfLoggedIn,
        controller.toggleNotifications);

    router.post('/api/users/me/blocked',
        authHandler.checkIfLoggedIn,
        controller.getBlockedUsers);

    router.post('/api/users/me/requests',
        authHandler.checkIfLoggedIn,
        controller.getUserPendingRequests);

    router.post('/api/app/users/:id/events/:type',
        authHandler.checkIfLoggedIn,
        controller.getUserEvents);

    router.post('/api/app/users/:id/badges',
        controller.getUserBadges);

    router.post('/api/users/:id/followers',
        authHandler.checkIfLoggedIn,
        authHandler.isAllowedByUser,
        controller.getUserFollowers);

    router.post('/api/users/:id/following',
        authHandler.checkIfLoggedIn,
        authHandler.isAllowedByUser,
        controller.getUserFollowing);

    router.post('/api/users/me/requests/accept/:id',
        authHandler.checkIfLoggedIn,
        controller.acceptRequest);

    router.post('/api/users/me/requests/decline/:id',
        authHandler.checkIfLoggedIn,
        controller.declineRequest);

    router.post('/api/users/interests',
        authHandler.checkIfLoggedIn,
        controller.updateInterests);

    router.post('/api/users/setBio',
        authHandler.checkIfLoggedIn,
        controller.updateAbout);

    router.post('/api/users/list',
        authHandler.checkIfLoggedIn,
        controller.getUsers
    );

    router.post('/api/users/avatar',
        authHandler.checkIfLoggedIn,
        upload.single('avatar'),
        controller.updateAvatar);

    router.post('/api/users/:id/follow',
        authHandler.checkIfLoggedIn,
        controller.followUser);

    router.post('/api/users/:id/block',
        authHandler.checkIfLoggedIn,
        controller.blockUser);

};
