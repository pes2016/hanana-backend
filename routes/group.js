'use strict'

var authHandler = require('../handlers/auth-handlers');
var controller = require('./../controllers/group');

module.exports = function (router) {

    router.post('/api/app/groups/:id/share',
        authHandler.checkIfLoggedIn,
        controller.shareGroup);

    router.get('/api/groups/:id',
        authHandler.checkIfLoggedIn,
        controller.getGroup);

    router.post('/api/groups/discover',
        authHandler.checkIfLoggedIn,
        controller.discoverGroups);

    router.post('/api/groups/:id/followers',
        authHandler.checkIfLoggedIn,
        controller.getFollowers);

    router.post('/api/groups/:id/follow',
        authHandler.checkIfLoggedIn,
        controller.toggleFollow);
};
