'use strict';

var authHandler = require('../handlers/auth-handlers');
var controller = require('./../controllers/admin');

var ImageStorage = require('../handlers/ImageStorage');
var multer = require('multer');
var directoryPath = require('../config.js').badgeImageStorage;
var storage = new ImageStorage({
    directoryPath: directoryPath,
    destination: function (req, file, cb) {
        cb(null, directoryPath);
    },

    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
});
var upload = multer({ storage: storage });

module.exports = function (router) {

    router.get('/api/admin/premiumUsers',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfAdmin,
        controller.getPremiumUsers);

    router.get('/api/admin/premiumUsers/:id',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfAdmin,
        controller.getPremiumUser);

    router.post('/api/admin/notify/',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfAdmin,
        controller.notifyUsers);

    router.post('/api/admin/premiumUsers/:id',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfAdmin,
        controller.editPremiumUser);

    router.delete('/api/admin/removePremiumUser/:id',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfAdmin,
        controller.removePremiumUser);

    router.post('/api/admin/resetPassword/:id',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfAdmin,
        controller.resetPassword);

    router.post('/api/admin/registerPremium',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfAdmin,
        controller.registerPremiumUser);

    router.get('/api/admin/badges',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfAdmin,
        controller.getBadges);

    router.get('/api/admin/badges/:id',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfAdmin,
        controller.getBadge);

    router.post('/api/admin/badges',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfAdmin,
        upload.single('image'),
        controller.createBadge);

    router.post('/api/admin/badges/:id',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfAdmin,
        upload.single('image'),
        controller.editBadge);

    router.get('/api/admin/triggers',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfAdmin,
        controller.getTriggers);

    router.get('/api/admin/complaints',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfAdmin,
        controller.getComplaints);

    router.delete('/api/admin/discardComplaint/:id',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfAdmin,
        controller.discardComplaint);

    router.delete('/api/admin/acceptComplaint/:id',
        authHandler.checkIfLoggedIn,
        authHandler.checkIfAdmin,
        controller.acceptComplaint);
};
