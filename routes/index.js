'use strict'
module.exports = function (express) {
    let router = express.Router();

    // User routes
    require('./user.js')(router);
    require('./interest.js')(router);

    // Admin routes
    require('./admin.js')(router);

    // Premium routes
    require('./premium.js')(router);

    // Group routes
    require('./group.js')(router);

    //Event routes
    require('./event.js')(router);

    //Complaint routes
    require('./complaint.js')(router);

    return router;
};
