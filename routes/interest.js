'use strict'
var authHandler = require('../handlers/auth-handlers');
var controller = require('./../controllers/interest');

module.exports = function (router) {
    router.get('/api/interests', authHandler.checkIfLoggedIn, controller.listAll);
};
