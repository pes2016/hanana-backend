'use strict'
var authHandler = require('../handlers/auth-handlers');
var controller = require('./../controllers/complaint');

module.exports = function (router) {

    router.post('/api/complaints/user/:id',
        authHandler.checkIfLoggedIn,
        controller.reportUser);

    router.post('/api/complaints/group/:id',
        authHandler.checkIfLoggedIn,
        controller.reportGroup);

    router.post('/api/complaints/event/:id',
        authHandler.checkIfLoggedIn,
        controller.reportEvent);

};
