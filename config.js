'use strict'

module.exports = {
  secret: 'HUEHUEHUEHEUEHUEHEUHEUHEUHEUEHUEHUE',
  expiresTime: 86400,
  publicPath: '/',
  staticPath: '../hanana-web/',
  appPath: '../hanana-web/app',
  dbUri: process.env.MONGO_URL || 'mongodb://localhost/hanana',
  port: process.env.PORT || 4000,
  ip: process.env.IP || 'localhost',
  apiKeyMailer: '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',
  groupImageStorage: 'groupImages/',
  eventImageStorage: 'eventImages/',
  badgeImageStorage: 'badgeImages/',
  QRImageStorage: 'qrCodes/'
};
